#ifndef COMMON_H    
#define COMMON_H_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> // write, close, etc
#include <ctype.h> // isdigit()
#include <arpa/inet.h> // inet_pton()
#include <sys/un.h> // UNIX addresses
#include <net/if.h> // INET6
#include <netinet/in.h> // INET6
#include <pthread.h>
#include <errno.h>
#include <stdatomic.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <sqlite3.h>
#include <sys/stat.h> // stat for file size
#include <fcntl.h> // open() for file management


#define SA struct sockaddr
#define MAXLINE 4096

    int isValidIPAddress(char *ipAddr);
    int isValidIPV6Address(char *ipAddr);
    int my_strlen(char *c);
    void clearBuffer();
    int safeGetString(char *string, int max);
    int isValidFilename(char *string);
    char const * errnoname(int errno_);

#endif 