#include "Common.h"

void VerifyUNIXClientArguments(int argc, char *argv[]);

int main(int argc, char *argv[])
{
    // Variables
    //--------------------------------------------------------------------------------------------------------------------------------------------
    // Configure socket
    int sockfd; // File Descriptor for the socket
    struct sockaddr_un servaddr; // Structure to specify the server address

    // Send/Receive
    long unsigned int sendBytes; // Number of Bytes to send
    long int readBytes; // Number of Bytes to send
    long int WriteReturnValue;

    // Get messages from STDIN to send/receive to the server
    char recvline[MAXLINE];   // Entered via stdin
    char string[MAXLINE];   // Entered via stdin
    char aux[MAXLINE];      // Without /n
    char sendmsg[MAXLINE];
    char endOfMsg[7] = "\n";

    //unsigned int sleepTime;
    //--------------------------------------------------------------------------------------------------------------------------------------------

    VerifyUNIXClientArguments(argc, argv);

    // Create Socket, AF_UNIX => Within this system, SOCK_STRAM = Stream Socket, 0 = Default protocol (TCP)
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
    {
        printf("Failed to create socket\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Handle the server address (file path) with which you want to connect
    //--------------------------------------------------------------------------------------------------------------------------------------------
    memset((char *)&servaddr, '\0', sizeof(servaddr));
    servaddr.sun_family = AF_UNIX;
    strcpy(servaddr.sun_path, argv[1]);
    //servlen = strlen(servaddr.sun_path) + sizeof(servaddr.sun_family);
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Connect to the server and handle errors
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(connect(sockfd, (SA *)&servaddr, sizeof(servaddr)) < 0)
    {
        printf("Failed to connect to the server\n");
        exit(EXIT_FAILURE); 
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Receive the message to send and the sleep time by arguments
    //--------------------------------------------------------------------------------------------------------------------------------------------
    strcpy(string, "SELECT * FROM Cars");
    //sleepTime = (unsigned int)atoi(argv[2]);
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Build the message to send
    //--------------------------------------------------------------------------------------------------------------------------------------------
    string[strcspn(string, "\n")] = 0;      
    strcpy(aux, string);                     // Save string without \n in aux evaluate the while
    strcat(string, endOfMsg);                // Add end of message (simulated as the end of an HTTP message)
    
    memset(sendmsg, 0, MAXLINE); 
    strcat(sendmsg, "Type A | ");
    strcat(sendmsg, string);
    sendBytes = strlen(sendmsg); // Calculate the number of characters in the message
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    //while(1)
    //{
        // sleep time received
        //----------------------------------------------------------------------------------------------------------------------------------------
        //usleep(sleepTime);
        //----------------------------------------------------------------------------------------------------------------------------------------

        // Send the message
        //----------------------------------------------------------------------------------------------------------------------------------------
        WriteReturnValue = write(sockfd, sendmsg, sendBytes);
        if((WriteReturnValue == -1) || ((long unsigned int)WriteReturnValue != sendBytes))
        {
            printf("Failed to send message\n");
            exit(EXIT_FAILURE); 
        }
        //----------------------------------------------------------------------------------------------------------------------------------------

        // Receive and print the response
        //----------------------------------------------------------------------------------------------------------------------------------------
        memset(recvline, 0, MAXLINE); 
        readBytes = 0;
        while((recvline[readBytes - 1] != '\n') && (recvline[readBytes - 2] != '\r')) // Detect end of the query
        {
            while(readBytes <= 0) // Wait until there is data
            {
                readBytes = recv(sockfd, recvline, MAXLINE-1, MSG_DONTWAIT);
                if(readBytes > 0)
                {
                    // Here you can save the data somewhere
                    //printf("Receiving %ld bytes\n", readBytes);
                }
            }
        }
        printf("Received: \n%s\n", recvline);
        //----------------------------------------------------------------------------------------------------------------------------------------
    //}
    
    //--------------------------------------------------------------------------------------------------------------------------------------------
    close(sockfd);      // Close the socket
    exit(EXIT_SUCCESS); // Exit
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

void VerifyUNIXClientArguments(int argc, char *argv[])
{
    // Verify the number of arguments
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(argc != 2)  
    {
        printf("Incorrect number of arguments\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verify that the file name for the UNIX connection is correct
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((strlen(argv[1]) > MAXLINE) || (!isValidFilename(argv[1])))
    {
        printf("Incorrect file name\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verify that the amount to wait before sending again is correct
    //--------------------------------------------------------------------------------------------------------------------------------------------
    /*for(unsigned int i = 0; i < strlen(argv[2]); i ++)
    {
        if((isdigit(argv[2][i]) == 0)|| (atoi(argv[2]) <= 0)) 
        {                                                                
            printf("You must enter a correct amount\n");                
            exit(EXIT_FAILURE);
        }
    }*/                                           
    //--------------------------------------------------------------------------------------------------------------------------------------------
}
