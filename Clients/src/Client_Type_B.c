#include "Common.h"

void VerifyArgumentsClientINET6(int argc, char *argv[]);

int main(int argc, char *argv[])
{
    // Variables
    //--------------------------------------------------------------------------------------------------------------------------------------------
    // Configure socket
    int sockfd; // File Descriptor for the socket
    struct sockaddr_in6 servaddr; // Structure to specify the server address
    short unsigned int iport; // int to verify port number

    // Send/Receive
    long unsigned int sendBytes; // Number of Bytes to send
    long int readBytes; // Number of Bytes to send
    long int WriteReturnValue;

    // Get messages from STDIN to send/receive to the server
    char recvline[MAXLINE];   // Entered via stdin
    char string[MAXLINE];   // Entered via stdin
    char aux[MAXLINE];      // Without /n
    char sendmsg[MAXLINE];
    char endOfMsg[7] = "\n";
    char aux2[MAXLINE];      // Without /n
    //--------------------------------------------------------------------------------------------------------------------------------------------

    VerifyArgumentsClientINET6(argc, argv);
    iport = (short unsigned int)atoi(argv[2]);

    // Create Socket, AF_INET = Internet, SOCK_STRAM = Stream Socket, 0 = Default protocol (TCP)
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if ((sockfd = socket(AF_INET6, SOCK_STREAM, 0)) < 0)
    {
        printf("Failed to create socket\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Handle the server address with which you want to connect
    //--------------------------------------------------------------------------------------------------------------------------------------------
    // bzero(void *s, size_t n) eliminates the data in the n bytes of memory starting at the address pointed to by s, writing zero.
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin6_family = AF_INET6;
    // htons (host to network, short) converts any byte order to the standard network byte order. This way, it doesn't matter
    // what byte format the applications connecting to the socket have, they can communicate.
    servaddr.sin6_port = htons(iport);
    // int inet_pton(int af, const char *restrict src, void *restrict dst); Where af = Address Family, converts the string src into a network address structure 
    // (e.g., servaddr) with the format of af and then copies that structure to dst. This way, we get the address to which we want to connect through the arguments.
    if (inet_pton(AF_INET6, argv[1], &servaddr.sin6_addr) <= 0)
    {
        printf("Failed to convert IP address to binary\n");
        exit(EXIT_FAILURE);
    }
    servaddr.sin6_scope_id = if_nametoindex(argv[3]); // enp2s0
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Connect to the server and handle errors
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if (connect(sockfd, (SA *)&servaddr, sizeof(servaddr)) < 0)
    {
        printf("Failed to connect to the server\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Receive from stdin the messages to send or detect if you want to exit the program
    //--------------------------------------------------------------------------------------------------------------------------------------------
    while (strcmp(aux, "exit") != 0)
    {
        // Build the message to send
        //----------------------------------------------------------------------------------------------------------------------------------------
        memset(string, 0, MAXLINE);
        memset(sendmsg, 0, MAXLINE);
        strcat(sendmsg, "Type B | ");
        printf("Enter the message to send: ");
        //fgets(string, MAXLINE, stdin);            // Get string entered via stdin
        while (safeGetString(string, MAXLINE) == -1)
        {
            printf("Max character limit exceeded (%d), enter a message with correct size: ", MAXLINE - 2);
        }
        //strcat(string, "SELECT Name FROM Cars");
        string[strcspn(string, "\n")] = 0; // Remove \n to evaluate if "exit" has been entered
        /*if(!strcmp(string,"exit"))
        {
            printf("detected, exit\n");
            //break;
        }*/
        strcpy(aux, string);
        strcpy(aux2, string);                       // Save string without \n in aux evaluate the while
        strcat(string, endOfMsg);                // Add end of message (simulated as the end of an HTTP message)
        //printf("The message is %sAAA\n", string); // Print the final message. The AAAs are to see if there are line breaks in string

        // Send the message
        //----------------------------------------------------------------------------------------------------------------------------------------
        // Send the message:
        // ssize_t write(int fd, const void *buf, size_t count); Writes up to "count" Bytes from the buffer starting at *buf to the file referred to
        // by the file descriptor fd. Returns the number of Bytes written on success or -1 on error. It can also return a number of Bytes less than count
        // due to some interruption or some other reason, in which case it is also handled as an error
        strcat(sendmsg, string);
        sendBytes = strlen(sendmsg); // Calculate the number of characters in the message
        WriteReturnValue = write(sockfd, sendmsg, sendBytes);
        if ((WriteReturnValue == -1) || ((long unsigned int)WriteReturnValue != sendBytes))
        {
            printf("Failed to send message\n");
            exit(EXIT_FAILURE);
        }
        //----------------------------------------------------------------------------------------------------------------------------------------

        if (!strcmp(aux2, "exit"))
        {
            printf("detected, exit\n");
            break;
        }

        // Receive and print response
        //----------------------------------------------------------------------------------------------------------------------------------------
        memset(recvline, 0, MAXLINE);
        readBytes = 0;
        while ((recvline[readBytes - 1] != '\n') && (recvline[readBytes - 2] != '\r')) // Detect end of the query
        {
            while (readBytes <= 0) // Wait until there is data
            {
                readBytes = recv(sockfd, recvline, MAXLINE - 1, MSG_DONTWAIT);
                if (readBytes > 0)
                {
                    // Here you can save the data somewhere
                    //printf("Receiving %ld bytes\n", readBytes);
                }
            }
        }
        printf("Received: \n%s\n", recvline);
        //----------------------------------------------------------------------------------------------------------------------------------------

        // Verify read errors
        //----------------------------------------------------------------------------------------------------------------------------------------
        if (readBytes < 0)
        {
            printf("Read error\n");
            exit(EXIT_FAILURE);
        }
        //----------------------------------------------------------------------------------------------------------------------------------------
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------------------------------------------------
    close(sockfd);      // Close the socket
    exit(EXIT_SUCCESS); // Exit
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

void VerifyArgumentsClientINET6(int argc, char *argv[])
{
    // Verify number of arguments
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if (argc != 4)
    {
        printf("Incorrect number of arguments\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verify that the port entered in the arguments is correct
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for (unsigned int i = 0; i < strlen(argv[2]); i++)
    {
        if ((isdigit(argv[2][i]) == 0) || (atoi(argv[2]) <= 0) || (atoi(argv[2]) > 65535)) // Verify that the argument for the port has been
        {                                                                                 // entered as a number and not letters or special characters
            printf("You must enter a correct port\n");                                   // and that this number is >= 0 and < 65535
            exit(EXIT_FAILURE);
        }
    }                                           // The value is written in iport AFTER verifying that the value is correct since the entered
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verify that the entered interface consists only of letters and digits. If it passes this test, it still has to be a correct interface,
    // otherwise connect() will fail
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for (unsigned int i = 0; i < strlen(argv[3]); i++)
    {
        if (((isdigit(argv[3][i]) == 0) && (isalpha(argv[3][i]) == 0)) || strlen(argv[3]) > MAXLINE)
        {
            printf("You must enter a correct interface\n");
            exit(EXIT_FAILURE);
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
}
