#include "Common.h"

#define FILEPATH "test.db"

void downloadFile(int *socket, unsigned long int file_size);
void VerifyArgumentsClientINET(int argc, char *argv[]);
void testQuery();

int main(int argc, char *argv[])
{
    // Variables
    //--------------------------------------------------------------------------------------------------------------------------------------------
    // Configure socket
    int sockfd; // File Descriptor for the socket
    struct sockaddr_in servaddr; // Structure to specify the server address
    short unsigned int iport; // int to verify port number

    // Send/Receive
    long unsigned int sendBytes; // Number of Bytes to send
    long int readBytes; // Number of Bytes to send
    long int WriteReturnValue; // Variable to control the return value of write()

    // Get messages from STDIN to send/receive to the server
    char recvline[MAXLINE];  // Buffer to receive
    char sendmsg[MAXLINE];   // Buffer to send
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verify arguments and obtain the port number from them
    //--------------------------------------------------------------------------------------------------------------------------------------------
    VerifyArgumentsClientINET(argc, argv);
    iport = (short unsigned int)atoi(argv[2]); 
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Create Socket, AF_INET = Internet, SOCK_STRAM = Stream Socket, 0 = Default protocol (TCP)
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("Failed to create socket\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Configure address, family, and port of the server to connect to
    //--------------------------------------------------------------------------------------------------------------------------------------------
    // bzero(void *s, size_t n) eliminates the data in the n bytes of memory starting at the address pointed to by s, writing zero.
    bzero(&servaddr, sizeof(servaddr)); // Clean structure 
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(iport); // Load the port number into the structure (adjust to the standard byte order of the network using htons)
    if (inet_pton(AF_INET, argv[1], &servaddr.sin_addr) <= 0) // Verify and assign IPV4
    {
        printf("Failed to convert IP address to binary\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Connect to the server and handle errors
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if (connect(sockfd, (SA *)&servaddr, sizeof(servaddr)) < 0)
    {
        printf("Failed to connect to the server\n");
        exit(EXIT_FAILURE); 
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Build the message to send
    //--------------------------------------------------------------------------------------------------------------------------------------------
    memset(sendmsg, 0 , MAXLINE); 
    strcat(sendmsg, "Type C | Download File\n");

    // Send the message
    //--------------------------------------------------------------------------------------------------------------------------------------------
    sendBytes = strlen(sendmsg); // Calculate the number of characters in the message
    WriteReturnValue = write(sockfd, sendmsg, sendBytes);
    if ((WriteReturnValue == -1) || ((long unsigned int)WriteReturnValue != sendBytes))
    {
        printf("Failed to send message\n");
        exit(EXIT_FAILURE); 
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Receive and print response
    //--------------------------------------------------------------------------------------------------------------------------------------------
    memset(recvline, 0 , MAXLINE); 
    readBytes = 0;
    while ((recvline[readBytes - 1] != '\n') && (recvline[readBytes - 2] != '\r')) // Detect end of the query
    {
        while (readBytes <= 0) // Wait until there is data
        {
            readBytes = recv(sockfd, recvline, MAXLINE-1, MSG_DONTWAIT);
        }
    }
    //printf("Received size: %s\n", recvline);
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verify read errors
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if (readBytes < 0)
    {
        printf("Read error\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Notify received size
    //--------------------------------------------------------------------------------------------------------------------------------------------
    memset(sendmsg, 0 , MAXLINE); 
    strcat(sendmsg, "Size received\n");
    sendBytes = strlen(sendmsg); // Calculate the number of characters in the message
    //printf("The ack size is %ld\n", sendBytes);
    WriteReturnValue = write(sockfd, sendmsg, sendBytes);
    if ((WriteReturnValue == -1) || ((long unsigned int)WriteReturnValue != sendBytes))
    {
        printf("Failed to send message\n");
        exit(EXIT_FAILURE); 
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Get the file size from the message
    //--------------------------------------------------------------------------------------------------------------------------------------------
    char fileSize[MAXLINE];
    strcpy(fileSize, recvline);
    fileSize[strcspn(fileSize, "\n")] = 0; 
    long unsigned int File_Size = (long unsigned int)atoi(fileSize);
    //printf("The file size is %ld\n", File_Size);
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Download file
    //--------------------------------------------------------------------------------------------------------------------------------------------
    downloadFile(&sockfd, File_Size);
    //printf("File downloaded\n");
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // Test that the received file is the correct database
    //--------------------------------------------------------------------------------------------------------------------------------------------
    testQuery();
    //--------------------------------------------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------------------------------------------------
    close(sockfd);      // Close the socket
    exit(EXIT_SUCCESS); // Exit
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

void downloadFile(int *socket, unsigned long int file_size)
{
    // Variables
    //--------------------------------------------------------------------------------------------------------------------------------------------
    char recvline[MAXLINE];
    long int rcvBytes = 0;
    long int totalRcvBytes = 0;
    long int BytesWritten = 0;
    unsigned long int fileSIZE = file_size;
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Open/Create File
    //--------------------------------------------------------------------------------------------------------------------------------------------
    //int target = open(FILEPATH,O_RDWR | O_CREAT | O_APPEND, 0664);
    int target = open(FILEPATH, O_WRONLY | O_CREAT | O_TRUNC, 0644);
    if (target == -1)
    {
        printf("Error creating/reading the file\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    //printf("I'm here\n");
    // Receive and write to the file until all bytes have been received
    //--------------------------------------------------------------------------------------------------------------------------------------------
    while (fileSIZE != 0)
    {
        memset(recvline, 0, MAXLINE);

        rcvBytes = read(*socket, recvline, MAXLINE);       
        fileSIZE -= (unsigned long int)rcvBytes;
        totalRcvBytes += rcvBytes;
        //printf("Received %ld Bytes\n", totalRcvBytes);

        BytesWritten += write(target, recvline, (unsigned long int)rcvBytes);
        //printf("Written %ld Bytes\n", BytesWritten);
    }
    //printf("Finished writing the file\n");
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Close file and handle error
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if ((close(target) < 0))
    {
        printf("Error closing connection\n");
        exit(EXIT_FAILURE); 
    } 
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

void VerifyArgumentsClientINET(int argc, char *argv[])
{
    // Verify number of arguments
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if (argc != 3)  
    {
        printf("Incorrect number of arguments\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verify that the port entered in the arguments is correct
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for (unsigned int i = 0; i < strlen(argv[2]); i ++)
    {
        if ((isdigit(argv[2][i]) == 0) || (atoi(argv[2]) <= 0) || (atoi(argv[2]) > 65535)) // Verify that the port argument has been
        {                                                                                 // entered as a number and not letters or special characters
            printf("You must enter a correct port\n");                                   // and that this number is >= 0 and < 65535
            exit(EXIT_FAILURE);
        }
    }                                           // The value is written in iport AFTER verifying that the value is correct since the entered
    //                                             value in the argument can exceed the max value of the short unsigned int
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

void testQuery()
{
    sqlite3 *db;    // This is the database in the code
    //char *err_msg = 0;
    sqlite3_stmt *res;
    
    int rc = sqlite3_open("test.db", &db); // Open the database (file "test.db", if it doesn't exist, create it) (int rc is just to receive success or not)
    
    if (rc != SQLITE_OK)  // Handle error
    {
        fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
        printf("Unable to open the db. The error is %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        exit(EXIT_FAILURE);
    }

    printf("Performing query...\n"); 

    rc = sqlite3_prepare_v2(db, "SELECT Name FROM Cars", -1, &res, 0);  // Make SELECT query  

    if (rc != SQLITE_OK) {
        
        fprintf(stderr, "Failed to fetch data: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        exit(EXIT_FAILURE);
    }

    printf("Results obtained!\n");
    printf("Display results:\n");

    while (sqlite3_step(res) != SQLITE_DONE)
    {
        int i;
        int num_cols = sqlite3_column_count(res);
        //printf("There are %d columns\n", num_cols);
        for (i = 0; i < num_cols; i++)
		{
			switch (sqlite3_column_type(res, i))
			{
			case (SQLITE3_TEXT):
				printf("%s, ", sqlite3_column_text(res, i));
				break;
			case (SQLITE_INTEGER):
				printf("%d, ", sqlite3_column_int(res, i));
				break;
			case (SQLITE_FLOAT):
				printf("%g, ", sqlite3_column_double(res, i));
				break;
			default:
				break;
			}
		}
		printf("\n");
    }
}
