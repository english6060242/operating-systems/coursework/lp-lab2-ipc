#ifndef DBFUNCTIONS_H   
#define DBFUNCTIONS_H_H

void CreateAndFillDB(sqlite3 **db, char *err_msg);
void getQueryResponse(int rc, sqlite3 **db, char *querry, char *response, pthread_mutex_t *lock);
void AddMsgToDB(sqlite3 **db, char *mensaje, int *lastid);

#endif 