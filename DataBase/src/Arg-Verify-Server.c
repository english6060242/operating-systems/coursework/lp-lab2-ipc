#include "Common.h"

void verifyArguments(int argc, char *argv[])
{
    // Verify correct number of arguments, then perform verifications for the arguments of each protocol
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if (argc != 3)
    {
        printf("Incorrect number of arguments\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    verifyArgumentsINET(argv);
}

void verifyArgumentsINET(char *argv[])
{
    // Verify that the entered IPV4 is correct
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if (!isValidIPAddress(argv[1]))
    {
        printf("You must enter a correct IPV4 address\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verify that the entered port for IPV4 is composed of digits and has a correct value
    for (unsigned int i = 0; i < strlen(argv[2]); i++)
    {
        if ((isdigit(argv[2][i]) == 0) || (atoi(argv[2]) <= 0) || (atoi(argv[2]) > 65535))
        {
            // Verify that a correct port has been entered in the argument, composed of digits and not letters or special characters
            printf("You must enter a correct port\n");
            // and that this number is >= 0 and < 65535
            exit(EXIT_FAILURE);
        }
    } // The value is written to iport AFTER verifying that the value is correct
    //--------------------------------------------------------------------------------------------------------------------------------------------
}
