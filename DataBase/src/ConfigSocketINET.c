#include "Common.h"

void ServerConfigSocketINET(int *sock, struct sockaddr_in *servaddr, int iport, long unsigned int max, char *stringaddr)
{
    // Create Socket
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if ((*sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("Error creating socket - server\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Allow reusing the address
    //--------------------------------------------------------------------------------------------------------------------------------------------
    int yes = 1;

    if (setsockopt(*sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1)
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Configure the type, port, and address of the socket
    //--------------------------------------------------------------------------------------------------------------------------------------------
    bzero(servaddr, sizeof(*servaddr)); // Clear servaddr structure
    servaddr->sin_family = AF_INET;     // It is a socket to communicate with remote processes using IPV4
    servaddr->sin_addr.s_addr = inet_addr(stringaddr); // Get IP from the arguments
    servaddr->sin_port = htons((unsigned short int)iport); // Get port from the arguments
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Associate the socket with the specified address
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if ((bind(*sock, (SA *)servaddr, sizeof(*servaddr))) < 0)
    {
        printf("Error associating socket with address\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Listen on the socket
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if ((listen(*sock, (int)max)) < 0)
    {
        printf("Error listening for messages\n");
        exit(EXIT_FAILURE);
    }
    // listen(int sockfd, int backlog) sets the socket specified by the fd sockfd to
    // passive, meaning it waits for incoming connections to accept using the accept() call.
    // The backlog argument refers to the maximum number of pending connections (there is a queue for these).
    //--------------------------------------------------------------------------------------------------------------------------------------------
}
