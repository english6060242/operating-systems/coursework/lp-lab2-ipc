#include "Common.h"

void *INET_Server_Code(void *arg)
{
    struct INET_arg_struct *arguments = arg; // Retrieve arguments
    
    // Variables: Create / configure sockets
    //--------------------------------------------------------------------------------------------------------------------------------------------
    int listenfd; // File descriptor for the socket that listens for connections.
    int *connfd; // File descriptor for the socket from accept() (connection established)
    struct sockaddr_in servaddr; // Structure to specify the server address

    connfd = malloc((unsigned long int)arguments->maxClientes * sizeof(int));
    for(int i = 0; i < arguments->maxClientes; i++)
    {
        connfd[i] = -1;
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Variable to get the next id of obtained messages
    //--------------------------------------------------------------------------------------------------------------------------------------------
    int lastid = 0;
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // Variables: Create threads to handle connections | Allocate memory: As many TaskThreads, CountingThreads, and arguments as maxClientes
    //--------------------------------------------------------------------------------------------------------------------------------------------
    pthread_t *TaskThread;
    pthread_mutex_t Handler_lock = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_t Database_lock = PTHREAD_MUTEX_INITIALIZER;
    struct local_threads_arg_struct *Handler_Thread_Args;
    
    TaskThread = malloc((unsigned long int)arguments->maxClientes * sizeof(pthread_t)); 
    Handler_Thread_Args = malloc((unsigned long int)arguments->maxClientes * sizeof(struct local_threads_arg_struct));
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // Variables: Limit the number of handler threads that can be launched and determine the first available one. Allocate memory
    //--------------------------------------------------------------------------------------------------------------------------------------------
    long unsigned int CurrentAHAmount = 0; // Number of available handlers at a given time
    int *AvailableHandlers; // Array of int that serves as flags, if AvailableHandlers[i] == 1 -> handler i is available
    int nextHandler; // This int is evaluated with the first handler that is available at a given time

    AvailableHandlers = malloc((unsigned long int)arguments->maxClientes * sizeof(int)); // Allocate memory: The array will have as many elements as maxClientes
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Configure socket
    //--------------------------------------------------------------------------------------------------------------------------------------------
    ServerConfigSocketINET(&listenfd, &servaddr, arguments->IPV4_iport, (unsigned long int)arguments->maxClientes, arguments->IPV4_Server_Address);
    //--------------------------------------------------------------------------------------------------------------------------------------------
     
    // Initially, all handlers are available, fill the indicator array with "1" -> "Available"
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(int i = 0; i < arguments->maxClientes; i++)
    {
        AvailableHandlers[i] = 1;
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // The thread for INET is always waiting for connections and launching threads to handle them when established
    //--------------------------------------------------------------------------------------------------------------------------------------------
    while(*(arguments->salir) == 0) // If "salir" is entered in main, no more connections are expected
    {   
        // Get the number of available handlers. If there are already as many working threads as maxClientes, you must wait until one finishes
        //----------------------------------------------------------------------------------------------------------------------------------------
        pthread_mutex_lock(&Handler_lock); // Accessed in mutual exclusion, as handler threads modify AvailableHandlers by releasing a handler when they finish
        CurrentAHAmount = (long unsigned int)getAvailableHandlersAmount(AvailableHandlers, (unsigned long int)arguments->maxClientes);
        pthread_mutex_unlock(&Handler_lock);

        if(CurrentAHAmount == 0 || getFirstAvailableHandler(AvailableHandlers, (unsigned long int)arguments->maxClientes) < 0)
        {
            //printf("No available connections, wait until one becomes available\n");
            while(CurrentAHAmount == 0)
            {
                if(*(arguments->salir) == 0)
                {
                    pthread_mutex_lock(&Handler_lock);
                    CurrentAHAmount = (long unsigned int)getAvailableHandlersAmount(AvailableHandlers, (unsigned long int)arguments->maxClientes);
                    pthread_mutex_unlock(&Handler_lock);
                }
                else
                {
                    break;
                }
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------------
        
        if(*(arguments->salir) == 0)
        {
            // Knowing that there are available handlers, get the first available one in numerical order
            //------------------------------------------------------------------------------------------------------------------------------------
            pthread_mutex_lock(&Handler_lock);
            nextHandler = getFirstAvailableHandler(AvailableHandlers, (unsigned long int)arguments->maxClientes);
            pthread_mutex_unlock(&Handler_lock);
            //------------------------------------------------------------------------------------------------------------------------------------

            // Wait for a connection and handle the error in case it occurs. The fd conn[nextHandler] -> first available handler
            //------------------------------------------------------------------------------------------------------------------------------------
            int flags = fcntl(listenfd, F_GETFL, 0);       // Configure accept() to become
            fcntl(listenfd, F_SETFL, flags | O_NONBLOCK);  // non-blocking.

            while(connfd[nextHandler] < 0)
            {
                if(*(arguments->salir) == 0)
                {
                    connfd[nextHandler] = accept(listenfd, (SA *) NULL, NULL); // NULL -> It doesn't matter who connects, accept the connection
                    /*if(connfd[nextHandler] == -1) // I no longer check the error because my accept is non-blocking and I wait in this while
                    {                               // until a correct connection is established
                        printf("Error in accept()\n");
                        exit(EXIT_FAILURE);
                    }*/
                }
                else
                {
                    break;
                }
            }
            //------------------------------------------------------------------------------------------------------------------------------------
            
            // Now that there is a connection, fill in the arguments, launch the handler thread for this connection, and modify AvalableHandlers in mutual exclusion
            //------------------------------------------------------------------------------------------------------------------------------------
            //printf("Connection with %d established. Managed by thread %d\n",connfd[nextHandler],nextHandler); 
            
            Handler_Thread_Args[nextHandler].id = nextHandler;
            Handler_Thread_Args[nextHandler].ConnSocket = &(connfd[nextHandler]);
            Handler_Thread_Args[nextHandler].Handlers = AvailableHandlers;
            Handler_Thread_Args[nextHandler].Handler_lock = &Handler_lock;
            Handler_Thread_Args[nextHandler].Database_lock = &Database_lock;
            Handler_Thread_Args[nextHandler].lastid = &lastid;
            Handler_Thread_Args[nextHandler].db = arguments->db;
            Handler_Thread_Args[nextHandler].salir = arguments->salir;
            
            pthread_create(&(TaskThread[nextHandler]), NULL, Task, &(Handler_Thread_Args[nextHandler]));

            occupyHandler(AvailableHandlers, nextHandler, &Handler_lock);
            //------------------------------------------------------------------------------------------------------------------------------------
        }
        else
        {
            break;
        }   
    }

    // If you want to exit the server, wait for all threads launched by this INET thread to finish (these threads end if salir == 1)
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(*(arguments->salir) == 1)
    {
        for(int i = 0; i < arguments->maxClientes; i++)
        {
            pthread_join(TaskThread[i], NULL);
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Then close the fds, check for errors, and free all allocated memory
    //--------------------------------------------------------------------------------------------------------------------------------------------          
    if((close(listenfd) < 0)) // Close fd for listening connections
    {
        printf("Error closing listenfd\n");
        exit(EXIT_FAILURE); 
    }
    free(AvailableHandlers);   // Free array of available handler indicators
    free(TaskThread);          // Free array of threads to handle connections 
    free(connfd);              // Free array of fds for connections
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    return NULL;
}
