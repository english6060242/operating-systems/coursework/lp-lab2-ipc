#include "Common.h"

#define FILEPATH "test.db"

void* Task(void *arg)
{
    // Variables
    //------------------------------------------------------------------------------------------------------------------------------------------------
    struct local_threads_arg_struct *arguments = arg;
    
    char recvline[MAXLINE+1]; // Buffer to receive. It's MAXLINE + 1 because...
    char aux[MAXLINE];         // Auxiliary to remove the checksum and end of message
    char rawmsg[MAXLINE] = ""; // Message without checksum or end of line
    char fileSize[MAXLINE] = ""; // Build message to send file size
    char buffer[MAXLINE] = ""; // Send file
    long int readBytes; // Send file
    long int writeBytes; // Send file

    char recvline2[MAXLINE+1]; // Receive ack
    long int readBytes2; // Receive ack

    int rc = 0; // For success or error of DB operations
    char querry[MAXLINE] = "";
    char response[MAXLINE];
    //------------------------------------------------------------------------------------------------------------------------------------------------
    
    while(*(arguments->salir) == 0) 
    {
        // Read message: Initially, readBytes is zero. As long as you don't want to exit, call recv. If readBytes > 0 -> data has been received.
        //--------------------------------------------------------------------------------------------------------------------------------------------
        readBytes = 0;
        while(readBytes <= 0)
        {
            if(*(arguments->salir) == 0) 
            {
                readBytes = recv(*(arguments->ConnSocket), recvline, MAXLINE-1, MSG_DONTWAIT);
            }
            else  // If time runs out or a reading error occurs (which can be due to a timeout), exit
            {
                /*if(readBytes < 0)
                {
                    // EAGAIN errors are received due to timeout
                    printf("Read error in task %d\n", arguments->id);
                    printf("The errno is %s\n", errnoname((int)errno));
                } */
                if(close(*(arguments->ConnSocket)) < 0)
                {
                    printf("Error closing connection %d\n", *(arguments->ConnSocket));
                }
                break;
            }
        }
        //--------------------------------------------------------------------------------------------------------------------------------------------

        // Once the message is received (as long as it has not been indicated to exit), determine the type of client it comes from and resolve accordingly
        //--------------------------------------------------------------------------------------------------------------------------------------------
        if(*(arguments->salir) == 0)
        {
            while(readBytes > 0)
            {
                if(*(arguments->salir) == 0)
                {
                    if(recvline[readBytes - 1] == '\n') // The end of HTTP messages usually has \n\r\n\r, upon detecting \n, we can infer that it is the end of the message.
                    {                                   // Clear the content of the string (place zeros) so that no part of previous messages remains
                        //----------------------------------------------------------------------------------------------------------------------------
                        memset(aux, 0, MAXLINE);    
                        memset(rawmsg, 0, MAXLINE);
                        //----------------------------------------------------------------------------------------------------------------------------          

                        // Get the query without the message type or '\n' and get the message type in a char 'a', 'b', or 'c'
                        //----------------------------------------------------------------------------------------------------------------------------
                        strcpy(aux, recvline);
                        strcpy(rawmsg, aux);
                        rawmsg[strcspn(rawmsg, "\n")] = 0;
                        char t = getTypeAndMessage(aux, querry);
                        //----------------------------------------------------------------------------------------------------------------------------

                        // Add the received message to the messages table in the DB
                        //----------------------------------------------------------------------------------------------------------------------------
                        char msgAddDB[MAXLINE];
                        strcpy(msgAddDB, rawmsg);
                        msgAddDB[strcspn(msgAddDB, "\n")] = 0;
                        pthread_mutex_lock(arguments->Database_lock);
                        AddMsgToDB(arguments->db, msgAddDB, arguments->lastid);
                        pthread_mutex_unlock(arguments->Database_lock);
                        //----------------------------------------------------------------------------------------------------------------------------

                        // If the message comes from a type 'a' or 'b' client, perform the query and send the response
                        //----------------------------------------------------------------------------------------------------------------------------
                        if((t == 'a') || (t == 'b'))
                        {
                            //printf("Performing query...\n"); 
                            getQueryResponse(rc, arguments->db, querry, response, arguments->Database_lock);
                            //printf("Got from the query: %s\n", response);
                            strcat(response, "\r\n"); // Add \r\n to detect the end of the query from the server and the client
                            writeBytes = write(*(arguments->ConnSocket), response, strlen(response)); // Send response
                            if(writeBytes < 0) // Handle error
                            {
                                printf("Write error\n");
                                exit(EXIT_FAILURE); 
                            }
                        }
                        //----------------------------------------------------------------------------------------------------------------------------

                        // If the message comes from a type 'c' client, the handshake must be performed: size-> ; <-ack ; -> file
                        //----------------------------------------------------------------------------------------------------------------------------
                        if(t == 'c')
                        {
                            // Get the size of the file in bytes
                            //------------------------------------------------------------------------------------------------------------------------
                            struct stat sb;
                            long int fileBytes;

                            if(stat(FILEPATH, &sb))
                            {
                                perror("stat");
                                exit(EXIT_FAILURE);
                            }
                            else
                            {
                                fileBytes = sb.st_size;
                            }
                            //------------------------------------------------------------------------------------------------------------------------
                            
                            // Send file size
                            //------------------------------------------------------------------------------------------------------------------------
                            sprintf(fileSize, "%ld", fileBytes);
                            strcat(fileSize, "\r\n");
                            writeBytes = write(*(arguments->ConnSocket), fileSize, strlen(fileSize)); 
                            if(writeBytes < 0)
                            {
                                printf("Write error\n");
                                exit(EXIT_FAILURE); 
                            }
                            //------------------------------------------------------------------------------------------------------------------------

                            // Receive size ack
                            //------------------------------------------------------------------------------------------------------------------------
                            readBytes2 = 0;
                            memset(recvline2, 0, MAXLINE); 
                            while(readBytes2 <= 0)
                            {
                                if(*(arguments->salir) == 0) 
                                {
                                    readBytes2 = recv(*(arguments->ConnSocket), recvline2, MAXLINE-1, MSG_DONTWAIT);
                                }
                                else
                                {
                                    break;
                                }
                            }
                            // printf("Received ack: %s", recvline2);
                            //------------------------------------------------------------------------------------------------------------------------

                            // Send File
                            //------------------------------------------------------------------------------------------------------------------------
                            int input_file = open(FILEPATH, O_RDONLY); // Open file
                            long int sentBytes = 0;
                    
                            while (fileBytes > 0) 
                            {
                                if(*(arguments->salir) == 0) 
                                {
                                    // Read data into buffer.  We may not have enough to fill up buffer, so we
                                    // store how many bytes were actually read in bytes_read.
                                    long int bytes_read = read(input_file, buffer, sizeof(buffer));
                                    if (bytes_read < 0) // handle errors
                                    {
                                        printf("Error reading file\n");
                                        exit(EXIT_FAILURE);
                                    }

                                    long int bytes_written = write(*(arguments->ConnSocket), buffer, (unsigned long int)bytes_read);
                                    if (bytes_written <= 0) 
                                    {
                                        printf("Error sending\n");
                                        exit(EXIT_FAILURE);
                                    }

                                    fileBytes -= bytes_written;
                                    sentBytes += bytes_written;
                                    //printf("I have sent %ld Bytes\n", sentBytes);
                                }
                                else
                                {
                                    break;
                                }
                            }
                            close(input_file);
                            //------------------------------------------------------------------------------------------------------------------------
                        }
                        //----------------------------------------------------------------------------------------------------------------------------
                        
                        // If the message does not have the format TYPE A | message or TYPE B | Message or TYPE C | Message, the client type is not recognized
                        //----------------------------------------------------------------------------------------------------------------------------
                        if(t == 'd')
                        {
                            printf("The type of client has not been specified");
                        }
                        //----------------------------------------------------------------------------------------------------------------------------

                        readBytes = 0;
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        //--------------------------------------------------------------------------------------------------------------------------------------------        
    }
    
    freeHandler(arguments->Handlers, arguments->id, arguments->Handler_lock);
    //printf("Task ends: %d\n",arguments->id);
    return NULL;
}
