#include "Common.h"

void CreateAndFillDB(sqlite3 **db, char *err_msg)
{    
    // Int to receive success or error from sqlite3_open,...
    int rc;

    // Open the database (file "test.db", creates if not exists) 
    //--------------------------------------------------------------------------------------------------------------------------------------------
    rc = sqlite3_open("test.db", db);
    
    if (rc != SQLITE_OK)  // Handle error
    {
        fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(*db));
        sqlite3_close(*db);
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // Fill the database with some table(s)
    //--------------------------------------------------------------------------------------------------------------------------------------------
    char *sql = "DROP TABLE IF EXISTS Cars;" 
                "CREATE TABLE Cars(Id INT, Name TEXT, Price INT);" 
                "INSERT INTO Cars VALUES(1, 'Audi', 52642);"  
                "INSERT INTO Cars VALUES(2, 'Mercedes', 57127);" 
                "INSERT INTO Cars VALUES(3, 'Skoda', 9000);" 
                "INSERT INTO Cars VALUES(4, 'Volvo', 29000);" 
                "INSERT INTO Cars VALUES(5, 'Bentley', 350000);" 
                "INSERT INTO Cars VALUES(6, 'Citroen', 21000);" 
                "INSERT INTO Cars VALUES(7, 'Hummer', 41400);" 
                "INSERT INTO Cars VALUES(8, 'Volkswagen', 21600);"
                "INSERT INTO Cars VALUES(9, 'Ford', 12342);"
                "DROP TABLE IF EXISTS Mensajes;" 
                "CREATE TABLE Mensajes(Id INT, Msg TEXT);"; // Fill a string with the data to be entered into the database

    printf("Filling the database...\n");
    
    rc = sqlite3_exec(*db, sql, 0, 0, &err_msg); // Save that data into the "db" database
    
    if (rc != SQLITE_OK ) // Handle error
    {
        fprintf(stderr, "SQL error: %s\n", err_msg);
        sqlite3_free(err_msg);        
        sqlite3_close(*db);
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

void getQueryResponse(int rc, sqlite3 **db, char *querry, char *response, pthread_mutex_t *lock)
{
    sqlite3_stmt *res;
    char aux[MAXLINE];
    
    // Clear the buffer containing the response
    //--------------------------------------------------------------------------------------------------------------------------------------------
    memset(response, 0, MAXLINE);  
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // Perform a query with the received message
    //--------------------------------------------------------------------------------------------------------------------------------------------
    pthread_mutex_lock(lock);
    rc = sqlite3_prepare_v2(*db, querry, -1, &res, 0);  // Make a SELECT query  
    pthread_mutex_unlock(lock);
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Handle errors
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if (rc != SQLITE_OK) 
    {
        //fprintf(stderr, "Failed to fetch data: %s\n", sqlite3_errmsg(*db));
        sprintf(response, "Failed to fetch data: %s\n", sqlite3_errmsg(*db)); // If the query fails, send a message indicating the failure
        //sqlite3_close(*db);
        //exit(EXIT_FAILURE);
    }
    else
    {
        // Copy results into the "response" string
        //----------------------------------------------------------------------------------------------------------------------------------------
        //printf("Results obtained!\n");

        while(sqlite3_step(res) != SQLITE_DONE)
        {
            int i;
            int num_cols = sqlite3_column_count(res);
            //printf("There are %d columns\n", num_cols);
            for (i = 0; i < num_cols; i++)
            {
                switch (sqlite3_column_type(res, i))
                {
                case (SQLITE3_TEXT):
                    sprintf(aux, "%s, ", sqlite3_column_text(res, i));
                    strcat(response, aux);
                    break;
                case (SQLITE_INTEGER):
                    sprintf(aux, "%d, ", sqlite3_column_int(res, i));
                    strcat(response, aux);
                    break;
                case (SQLITE_FLOAT):
                    sprintf(aux, "%g, ", sqlite3_column_double(res, i));
                    strcat(response, aux);
                    break;
                default:
                    break;
                }
            }
            sprintf(aux, "\n");
            strcat(response, aux);
            //------------------------------------------------------------------------------------------------------------------------------------
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

void AddMsgToDB(sqlite3 **db, char *mensaje, int *lastid)
{
    // Int to receive success or error from sqlite3_open,...
    int rc;
    char comando[MAXLINE]; 
    char *err_msg = 0;

    // Open the database (file "test.db", creates if not exists) 
    //--------------------------------------------------------------------------------------------------------------------------------------------
    rc = sqlite3_open("test.db", db); // Open the database (file "test.db", creates if not exists) (int rc is only to receive success or not)
    
    if (rc != SQLITE_OK)  // Handle error
    {
        fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(*db));
        sqlite3_close(*db);
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    sprintf(comando, "INSERT INTO Mensajes VALUES(%d, '%s');", (*(lastid) + 1), mensaje);
    
    rc = sqlite3_exec(*db, comando, 0, 0, &err_msg); // Save that data into the "db" database
    
    if (rc != SQLITE_OK ) // Handle error
    {
        fprintf(stderr, "SQL error: %s\n", err_msg);
        sqlite3_free(err_msg);        
        sqlite3_close(*db);
        exit(EXIT_FAILURE);
    }

    *(lastid) = *(lastid) + 1;
    //--------------------------------------------------------------------------------------------------------------------------------------------
}
