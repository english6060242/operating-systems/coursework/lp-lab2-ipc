# lp-lab2-IPC : Three-Tier Client-Server Architecture

## Overview
This project implements a three-tier client-server architecture utilizing IPC mechanisms in C. The system comprises a server, multiple clients, and a SQLite database to facilitate interprocess communication. The clients interact with the server to execute custom queries, and the server manages communication between clients and the database.

![Block Diagram](img/connectionPool.png)

## Get Started
```
    cd existing_repo
    git remote add origin https://gitlab.com/english6060242/operating-systems/coursework/lp-lab2-ipc.git
    git branch -M main
    git push -uf origin main
```

## Project Structure
The project is divided into three main components:

### 1. Server
- **Functionality:**
  - Maintains five concurrent connections with the database throughout its lifecycle.
  - Manages client connections and disconnections.
  - Acts as a relay for user messages to the database.
  - Implements custom logic and design as per student requirements.

### 2. Clients
- **Three Types of Clients:**
  1. **Client A:**
     - Periodically sends a predefined query and displays the result.
  2. **Client B:**
     - Accepts user input for queries via CLI and displays the results.
  3. **Client C:**
     - Downloads a file from the database to the local host.

### 3. Database
- **SQLite Database:**
  - Stores messages sent by clients in a dedicated "Messages" table.
  - Utilizes IPC mechanisms to handle connections with the server.

## Compilation and Usage
- Compile the code with the following flags: `-Wall -Pedantic -Werror -Wextra -Wconversion -std=gnu11`
- Ensure proper memory management and modular code structure.
- Use the provided `Makefile` for compilation.

## Requirements
- All code should be free of errors and warnings.
- Memory management should adhere to best practices.
- Code should be modular, well-organized, and follow a consistent style.
- Proper handling of errors is mandatory.
- No use of features flagged by `cppcheck`.

## How to Run
1. Clone the repository: `git clone https://gitlab.com/english6060242/operating-systems/coursework/lp-lab2-ipc.git`
2. Use three terminals and navigate to DataBase, Server and Clients respectively.
3. On each terminal, compile the code: `make`
4. Run 'make test' for the DataBase and then the Server.
5. When it comes to the client, either use the individual make commands or run the bashCode.sh file (manage permissions first).

## Report
For a detailed explanation of the project development, design choices, and justification, refer to the provided report document in the repository.

## License
This project is licensed under the [MIT License](LICENSE.md).