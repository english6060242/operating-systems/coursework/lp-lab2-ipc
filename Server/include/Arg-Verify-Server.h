#ifndef SERVERARGVERIFY_H    // Start of the ifdef guard
#define SERVERARGVERIFY_H

void verifyArguments(int argc, char *argv[]);
void verifyArgumentsINET(char *argv[]);
void verifyArgumentsUNIX(char *argv[]);
void verifyArgumentsINET6(char *argv[]);

#endif // End of the ifdef guard
