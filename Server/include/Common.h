#ifndef COMMON_H     // Start of the ifdef guard
#define COMMON_H_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> // write, close, etc
#include <ctype.h> // isdigit()
#include <arpa/inet.h> // inet_pton()
#include <sys/un.h> // UNIX addresses
#include <net/if.h> // INET6
#include <netinet/in.h> // INET6
#include <pthread.h>
#include <errno.h>
#include <stdatomic.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <sqlite3.h>
#include <sys/stat.h> // stat to get the file size
#include <fcntl.h> // open() for file management

#endif // End of the ifdef guard