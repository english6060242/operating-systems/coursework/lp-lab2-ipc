#ifndef COMMONFUNCTIONS_H   
#define COMMONFUNCTIONS_H_H

    int isValidIPAddress(char *ipAddr);
    int isValidIPV6Address(char *ipAddr);
    int my_strlen(char *c);
    void clearBuffer();
    int safeGetString(char *string, int max);
    int isValidFilename(char *string);
    void occupyHandler(int *Handlers, int i, pthread_mutex_t *lock);
    void releaseHandler(int *Handlers, int i, pthread_mutex_t *lock);
    int getFirstAvailableHandler(int *Handlers, long unsigned int maxHandlers, pthread_mutex_t *lock);
    int getAvailableHandlersAmount(int *Handlers, long unsigned int maxHandlers, pthread_mutex_t *lock);
    char getTypeAndMessage(char *string, char *msg);
    char const * errnoname(int errno_);
    
#endif 