#include "Common.h"
#include "CommonFunctions.h"
#include "DataStructure.h"
#include "Arg-Verify-Server.h"

void verifyArguments(int argc, char *argv[])
{
    // Verify the correct number of arguments, then perform the checks for the arguments of each protocol.
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if (argc != 10)
    {
        printf("Incorrect number of arguments\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    verifyArgumentsINET(argv);
    verifyArgumentsUNIX(argv);
    verifyArgumentsINET6(argv);

    // Verify that the entered number of clients is a positive number and less than the maximum allowed.
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for (unsigned int i = 0; i < strlen(argv[9]); i++)
    {
        if ((isdigit(argv[9][i]) == 0) || (atoi(argv[9]) < 0) || (atoi(argv[9]) > 10000) || strlen(argv[9]) > sizeof(int))
        { // Verify that no letters, special characters,
            printf("You must enter a correct port\n");
            exit(EXIT_FAILURE);
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

void verifyArgumentsINET(char *argv[])
{
    // Verify that the entered IPV4 is correct
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if (!isValidIPAddress(argv[1]))
    {
        printf("You must enter a correct IPV4 address\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verify that the entered port for IPV4 consists of digits and has a correct value
    for (unsigned int i = 0; i < strlen(argv[2]); i++)
    {
        if ((isdigit(argv[2][i]) == 0) || (atoi(argv[2]) <= 0) || (atoi(argv[2]) > 65535))
        { // Verify that the argument for the port contains
            printf("You must enter a correct port\n"); // only a number and no letters or special characters
            exit(EXIT_FAILURE);                         // and that this number is >= 0 and < 65535
        }
    }                                                    // The value is written to iport AFTER verifying that the value is correct and the value entered
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verify that the entered IPV4 is correct
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if (!isValidIPAddress(argv[7]))
    {
        printf("You must enter a correct IPV4 address\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verify that the entered port for IPV4 consists of digits and has a correct value
    for (unsigned int i = 0; i < strlen(argv[8]); i++)
    {
        if ((isdigit(argv[8][i]) == 0) || (atoi(argv[8]) <= 0) || (atoi(argv[8]) > 65535))
        { // Verify that the argument for the port contains
            printf("You must enter a correct port\n"); // only a number and no letters or special characters
            exit(EXIT_FAILURE);                         // and that this number is >= 0 and < 65535
        }
    }                                                    // The value is written to iport AFTER verifying that the value is correct and the value entered
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

void verifyArgumentsUNIX(char *argv[])
{
    // Verify that the file name for UNIX connection is correct
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if ((strlen(argv[3]) > MAXLINE) || (!isValidFilename(argv[3])))
    {
        printf("Incorrect file name\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

void verifyArgumentsINET6(char *argv[])
{
    // Verify that the entered IPV6 address is correct
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if (!isValidIPV6Address(argv[4]))
    {
        printf("You must enter a correct IPV6 address\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verify that the entered port for IPV4 consists of digits and has a correct value
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for (unsigned int i = 0; i < strlen(argv[5]); i++)
    {
        if ((isdigit(argv[5][i]) == 0) || (atoi(argv[5]) <= 0) || (atoi(argv[5]) > 65535))
        { // Verify that the argument for the port contains
            printf("You must enter a correct port\n"); // only a number and no letters or special characters
            exit(EXIT_FAILURE);                         // and that this number is >= 0 and < 65535
        }
    }                                                    // The value is written to iport AFTER verifying that the value is correct and the value entered
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verify that the entered interface consists only of letters and digits. If it passes this test, it still has to be a correct
    // interface; otherwise, bind() will fail.
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for (unsigned int i = 0; i < strlen(argv[6]); i++)
    {
        if ((isdigit((argv[6][i]) == 0) && (isalpha(argv[6][i]) == 0)) || strlen(argv[6]) > MAXLINE)
        {
            printf("You must enter a correct interface\n");
            exit(EXIT_FAILURE);
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
}
