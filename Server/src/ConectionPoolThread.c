#include "Common.h"
#include "CommonFunctions.h"
#include "DataStructure.h"
#include "ConectionThread.h"

void* ConectionPoolThreadCode(void * arg)
{
    // Crear e inicializar variables
    //--------------------------------------------------------------------------------------------------------------------------------------------
    struct Pool_arg_struct *arguments = arg;
    pthread_t *ConectionThreads;
    struct Conection_arg_struct *conArgs;
    int availableConections[5];
    pthread_mutexattr_t mta; // para configurar atributos de los locks
    pthread_mutex_t *conections_lock;
    pthread_mutex_t *con_list_lock;
    db_request_list **l;

    ConectionThreads = malloc((5 * sizeof(pthread_t)));
    conArgs = malloc((5 * sizeof(struct Conection_arg_struct)));
    conections_lock = malloc((5 * sizeof(pthread_mutex_t)));
    con_list_lock = malloc((5 * sizeof(pthread_mutex_t)));
    l = malloc ((5 * sizeof(db_request_list)));

    pthread_mutexattr_init(&mta); // Inicializar mta con los valores por defecto
    pthread_mutexattr_setrobust(&mta,PTHREAD_MUTEX_ROBUST); // Robusto: Si un hilo toma el lock y muere antes de liberarlo, se libera auto.
    pthread_mutexattr_settype(&mta,PTHREAD_MUTEX_RECURSIVE); // Recursivo: Esto soluciona el error en la syscall cuando se ejecuta lock() muy seguido
    
    for(int  i = 0; i < 5; i++)
    {
        l[i] = new_db_request_list();
    }

    for(int  i = 0; i < 5; i++)
    {
        pthread_mutex_init(&conections_lock[i],&mta); 
        pthread_mutex_init(&con_list_lock[i],&mta); 
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Inicialmente todas las conexiones estan disponibles
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(int  i = 0; i < 5; i++)
    {
        pthread_mutex_lock(&conections_lock[i]);
        availableConections[i] = 1;
        pthread_mutex_unlock(&conections_lock[i]);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Llenar argumentso para los hilos de conexiones
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(int  i = 0; i < 5; i++)
    {
        conArgs[i].id = i;
        strcpy(conArgs[i].IPV4_Server_Address,arguments->IPV4_Server_Address);
        conArgs[i].IPV4_iport = arguments->IPV4_iport;
        conArgs[i].availableConections = availableConections;
        conArgs[i].conections_lock = &conections_lock[i];
        conArgs[i].list_lock = &con_list_lock[i];
        conArgs[i].ack_arg = arguments->ack_arg;
        conArgs[i].list = l[i];
        conArgs[i].salir = arguments->salir;
    }

    
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // Lanzar hilos para enviar/recibir de las 5 conexiones
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(int  i = 0; i < 5; i++)
    {
        pthread_create(&ConectionThreads[i],NULL,ConectionThreadCode,&conArgs[i]);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    int isEmpty = 1; // Para verificar si la lista de requests
    int availableConAmount = 0;
    int nextConection = -1;
    
    while((*arguments->salir) == 0)
    {

            // Esperar hasta que algun TaskHandlingThread agregue un requests a la lista
            //------------------------------------------------------------------------------------------------------------------------------------
            pthread_mutex_lock(arguments->req_list_lock);
            isEmpty = isEmpty_db_request_list(arguments->list);
            pthread_mutex_unlock(arguments->req_list_lock);

            while(isEmpty)
            {
                if(((*arguments->salir) == 0))
                {
                    pthread_mutex_lock(arguments->req_list_lock);
                    isEmpty = isEmpty_db_request_list(arguments->list);
                    pthread_mutex_unlock(arguments->req_list_lock);
                }
                else
                {
                    break;
                }
            }
            //------------------------------------------------------------------------------------------------------------------------------------
            
            if((*arguments->salir) == 0)
            {
                // Esperar hasta que haya conexiones disponibles
                //--------------------------------------------------------------------------------------------------------------------------------
                while(availableConAmount == 0)
                {
                    if(((*arguments->salir) == 0))
                    {
                        availableConAmount = getAvailableHandlersAmount(availableConections, (long unsigned int)5,conections_lock);
                        /*if(availableConAmount == 0)
                        {
                            printf("Todas las conexiones de la DB estan ocupadas\n");
                        }*/
                    }
                    else
                    {
                        break;
                    }
                }
                //--------------------------------------------------------------------------------------------------------------------------------

                if(*(arguments->salir) == 0)
                {
                    // Obtener el num de la primera conexion disponible (en orden numerico) y agregar los requests a la lista del hilo correspondiente a esa
                    // conexion. Luego quitar el request de la lista intermedia perteneciente al pool.
                    //----------------------------------------------------------------------------------------------------------------------------
                    nextConection = getFirstAvailableHandler(availableConections, 5, conections_lock); // Obtener primera conexion disponible en ex mutua
                    availableConAmount = 0;
                    while(nextConection == -1)
                    {
                        nextConection = getFirstAvailableHandler(availableConections, 5, conections_lock);
                    }
                    
                    ocuparHandler(availableConections,nextConection,&conections_lock[nextConection]);

                    if(nextConection == 5)
                    {
                        printf("Hubo next con 5\n");
                        exit(0);
                    }

                    pthread_mutex_lock(arguments->req_list_lock);
                    int *auxConn = get_db_request(arguments->list, 0)->conn; // Obtener el num de socket de la conexion con el cliente que ha generado el req
                    char auxString[MAXLINE];
                    strcpy(auxString,get_db_request(arguments->list, 0)->sendmsg); // Copiar el mensaje del request
                    pthread_mutex_unlock(arguments->req_list_lock);
                    // Añadir a la lista (contenida en los param del hilo que corresponde a la conexion disponible obtenida) un nuevo request (identico al que 
                    // fue agregado a la lista del pool por TaskHandlingThread) 
                    
                    pthread_mutex_lock(&con_list_lock[nextConection]);
                    add_db_request(conArgs[nextConection].list,db_request_list_getNextID(conArgs[nextConection].list) ,auxConn ,auxString); 
                    pthread_mutex_unlock(&con_list_lock[nextConection]);
                    
                    pthread_mutex_lock(arguments->req_list_lock);
                    remove_req_list_head(arguments->list);
                    pthread_mutex_unlock(arguments->req_list_lock);
                    //----------------------------------------------------------------------------------------------------------------------------
                }
            }
            else
            {
                break;
            }
    }
    
    // Esperar a que terminen los 5 hilos que manejan conexiones contra la DB
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(int i = 0; i < 5; i++)
    {
        pthread_join(ConectionThreads[i],NULL);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    for(int i = 0; i < 5; i++)
    {
        pthread_mutex_destroy(&conections_lock[i]);
        pthread_mutex_destroy(&con_list_lock[i]);
    }

    free(ConectionThreads);
    free(conArgs);
    free(conections_lock);
    free(con_list_lock);
    free(l);

    return NULL;
}