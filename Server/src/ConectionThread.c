#include "Common.h"
#include "CommonFunctions.h"
#include "DataStructure.h"

void* ConectionThreadCode(void * arg)
{
    // Variables
    //------------------------------------------------------------------------------------------------------------------------------------------------
    int connfd;
    struct sockaddr_in servaddr;
    struct Conection_arg_struct *arguments = arg;
    char sendmsg[MAXLINE];
    char recvline[MAXLINE];

    char recvline2[MAXLINE+1];
    long int readBytes2;
    long unsigned int sendBytes; // Cantidad de Bytes a enivar
    long int readBytes; // Cantidad de Bytes a enivar
    long int WriteReturnValue;

    int isEmpty = 1;
    //------------------------------------------------------------------------------------------------------------------------------------------------

    //printf("Soy conection %d\n",arguments->id);
    //print_db_request_list(*arguments->list);
    
    // Crear Socket, AF_INET = Internet, SOCK_STRAM = Stream Socket, 0 = Protocolo por defecto (TCP) (Conexion contra la DB)
    //------------------------------------------------------------------------------------------------------------------------------------------------
    if((connfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("Falla al crear socket conPool\n");
        exit(EXIT_FAILURE);
    }
    *(arguments->ack_arg->ConnSocket) = connfd; // Guardar en los argumentos el num del socket para que sea accesible por TaskHandlingTHread
    //------------------------------------------------------------------------------------------------------------------------------------------------

     // Configurar estructura para la conexion
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //printf("Config con add: %s y puerto %d\n",arguments->IPV4_Server_Address,arguments->IPV4_iport);
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(arguments->IPV4_iport);
    if(inet_pton(AF_INET,arguments->IPV4_Server_Address,&servaddr.sin_addr) <= 0)
    {
        printf("Falla al convertir dir IP a binario conPool\n");
        exit(EXIT_FAILURE);
    }
    //----------------------------------------------------------------------------------------------------------------------------------------------------

    // Conectarse al servidor (DB) y handlear errores
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    if(connect (connfd, (SA *) &servaddr, sizeof(servaddr)) < 0)
    {
        printf("Falla al conectar con el servidor conection_thread %d\n",arguments->id);
        exit(EXIT_FAILURE); 
    }
    //----------------------------------------------------------------------------------------------------------------------------------------------------

    while(*(arguments->salir) == 0)
    {
        // Esperar hasta que haya requests en la cola para enviar a la db
        //------------------------------------------------------------------------------------------------------------------------------------------------
        pthread_mutex_lock(arguments->list_lock);
        isEmpty = isEmpty_db_request_list(arguments->list);
        pthread_mutex_unlock(arguments->list_lock);
        
        while(isEmpty)
        {   
            if(*(arguments->salir) == 0)
            {
                pthread_mutex_lock(arguments->list_lock);
                isEmpty = isEmpty_db_request_list(arguments->list);
                pthread_mutex_unlock(arguments->list_lock);
            }
            else
            {
                break;
            }
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------
        if(*(arguments->salir) == 0)
        {
            // Una vez que se ingresa un request, esta conexion se encuntra ocupada
            //--------------------------------------------------------------------------------------------------------------------------------------------
            
            //ocuparHandler(arguments->availableConections,arguments->id,arguments->conections_lock);
            //--------------------------------------------------------------------------------------------------------------------------------------------

            // Obtener info del request (mensaje, socket conectado con el cliente, etc) en ex mutua
            //--------------------------------------------------------------------------------------------------------------------------------------------

            db_request *req = malloc(sizeof(db_request));
            char t = 'd';

            pthread_mutex_lock(arguments->list_lock);
            req = get_db_request(arguments->list,0);
            //req = arguments->list->node;
            pthread_mutex_unlock(arguments->list_lock);
            if(req != NULL)
            {
                //printf("Soy conection %d el mensaje del req es %s\n",arguments->id,req->sendmsg);
                //hab = 0;
                //printf("Tengo que enviar el mensaje AA%sAA\n",req->sendmsg);
                t = getTypeAndMessege(req->sendmsg,sendmsg);
                memset(sendmsg, 0, MAXLINE); 
                strcpy(sendmsg,req->sendmsg); 
            }
            else
            {
                //printf("Req es NULL en conection %d\n",arguments->id);
                exit(0);
            }
            //--------------------------------------------------------------------------------------------------------------------------------------------

            // Clientes tipo A y B -> enviar la querry a la db, recibir y redireccionar la respuesta
            //--------------------------------------------------------------------------------------------------------------------------------------------
            if((t == 'a') || (t == 'b'))
            {
                // Obtener el length del mensaje, y enviarlo a la db
                //----------------------------------------------------------------------------------------------------------------------------------------
                if(strcmp(sendmsg,"Tipo B | salir\n"))
                {
                    sendBytes = strlen(sendmsg);
                    WriteReturnValue = 0;
                    while(WriteReturnValue <= 0) // Esperar hasta que se envien datos
                    {
                        
                        if(*(arguments->salir) == 0)
                        {
                            WriteReturnValue = send(connfd, sendmsg, sendBytes, MSG_DONTWAIT);
                        }
                        else
                        {
                            break;
                        }
                    }
                    //----------------------------------------------------------------------------------------------------------------------------------------

                    // Recibir la repsuesta de la db
                    //----------------------------------------------------------------------------------------------------------------------------------------
                    memset(recvline, 0 , MAXLINE); 
                    readBytes = 0;
                    while((recvline[readBytes - 1] != '\n') && (recvline[readBytes - 2] != '\r')) // Detectar final de la respuesta a la querry
                    {
                        if(*(arguments->salir) == 0)
                        {
                            while(readBytes <= 0) // Esperar hasta que haya datos
                            {
                                if(*(arguments->salir) == 0)
                                {
                                    readBytes = recv(connfd, recvline, MAXLINE-1, MSG_DONTWAIT);
                                }
                                else
                                {
                                    break;
                                }
                            }
                            if((recvline[readBytes - 1] == '\n') && (recvline[readBytes - 2] == '\r'))
                            {
                                break;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    //printf("Recibido de la DB: AA%sAA\n",recvline);
                    //----------------------------------------------------------------------------------------------------------------------------------------
                    
                    // Redireccionar el mensaje al cliente
                    //----------------------------------------------------------------------------------------------------------------------------------------
                    memset(sendmsg, 0 , MAXLINE); 
                    strcpy(sendmsg,recvline);
                    sendBytes = strlen(sendmsg); // Calcular la cantidad de caracteres del mensaje
                    WriteReturnValue = 0;
                    while(WriteReturnValue <= 0) // Esperar hasta que haya datos
                    {
                        if(*(arguments->salir) == 0) // ver
                        {
                            if(*(req->conn) > 0)
                            {
                                WriteReturnValue = send(*(req->conn), sendmsg, sendBytes, MSG_DONTWAIT);
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    //WriteReturnValue = write(*(req->conn), sendmsg, sendBytes);
                    if((WriteReturnValue == -1) || ((long unsigned int)WriteReturnValue != sendBytes))
                    {
                        printf("Falla al enviar mensaje en conection Thread %d, fd = %d\n",arguments->id,*(req->conn));
                        printf("El errno es %s\n",errnoname((int)errno));
                        exit(EXIT_FAILURE); 
                    }
                    //----------------------------------------------------------------------------------------------------------------------------------------
                    
                }
                if(t == 'a' || (t == 'b' && (!strcmp(sendmsg,"Tipo B | salir\n"))))
                {
                    if(close(*(req->conn)) < 0)
                    {
                        printf("Soy conection %d, Error al cerrar la conec %d\n",arguments->id,*(req->conn));
                        printf("El errno es %s\n",errnoname((int)errno));
                    }
                    else
                    {
                        //printf("Soy conection %d, cierro el fd %d\n",arguments->id,*(req->conn));
                        pthread_mutex_lock(arguments->list_lock);
                        *(req->conn) = -1;
                        pthread_mutex_unlock(arguments->list_lock);
                    }
                }
                // Al finalizar quitar el request de la lista y liberar el handler
                //----------------------------------------------------------------------------------------------------------------------------------------
                pthread_mutex_lock(arguments->list_lock);
                remove_req_list_head(arguments->list);
                pthread_mutex_unlock(arguments->list_lock);

                liberarHandler(arguments->availableConections,arguments->id,arguments->conections_lock);
                //----------------------------------------------------------------------------------------------------------------------------------------
            }
            //--------------------------------------------------------------------------------------------------------------------------------------------
                
            if(t == 'c')
            {
                //sendBytes = strlen(req->sendmsg); // Calcular la cantidad de caracteres del mensaje
                sendBytes = strlen(sendmsg); // Calcular la cantidad de caracteres del mensaje
                WriteReturnValue = 0;
                while(WriteReturnValue <= 0) // Esperar hasta que haya datos
                {
                    if(*(arguments->salir) == 0)
                    {
                        WriteReturnValue = send(connfd, sendmsg, sendBytes, MSG_DONTWAIT);
                    }
                    else
                    {
                        break;
                    }
                }

                // Recibir tam del archivo
                //----------------------------------------------------------------------------------------------------------------------------------------
                memset(recvline, 0 , MAXLINE); 
                readBytes = 0;
                while((recvline[readBytes - 1] != '\n') && (recvline[readBytes - 2] != '\r')) // Detectar final de la querry
                {
                    if(*(arguments->salir) == 0)
                    {   
                        while(readBytes <= 0) // Esperar hasta que haya datos
                        {
                            readBytes = recv(connfd, recvline, MAXLINE-1, MSG_DONTWAIT);
                        }
                        if((recvline[readBytes - 1] == '\n') && (recvline[readBytes - 2] == '\r'))
                        {
                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                //----------------------------------------------------------------------------------------------------------------------------------------
                    
                // Notificar tam recibido
                //----------------------------------------------------------------------------------------------------------------------------------------
                memset(sendmsg, 0 , MAXLINE); 
                strcat(sendmsg,"Tam recibido\n");
                sendBytes = strlen(sendmsg); // Calcular la cantidad de caracteres del mensaje
                WriteReturnValue = write(connfd, sendmsg, sendBytes);
                if((WriteReturnValue == -1) || ((long unsigned int)WriteReturnValue != sendBytes))
                {
                    printf("Falla al enviar mensaje\n");
                    printf("El errno es %s\n",errnoname((int)errno));
                    exit(EXIT_FAILURE); 
                }
                //----------------------------------------------------------------------------------------------------------------------------------------

                // Enviar tam del archivo
                //----------------------------------------------------------------------------------------------------------------------------------------
                WriteReturnValue = 0;
                sendBytes = strlen(recvline);
                while(WriteReturnValue <= 0) // Esperar hasta que haya datos
                {
                    if(*(arguments->salir) == 0)
                    {
                        WriteReturnValue = send(*(req->conn), recvline, sendBytes, MSG_DONTWAIT);
                    }
                    else
                    {
                        break;
                    }
                }
                //----------------------------------------------------------------------------------------------------------------------------------------

                // Recibir ack de tam
                //----------------------------------------------------------------------------------------------------------------------------------------
                readBytes2 = 0;
                memset(recvline2, 0, MAXLINE); 
                while(readBytes2 != 13)
                {
                    if(*(arguments->salir) == 0) 
                    {
                        readBytes2 = recv(*(req->conn), recvline2, MAXLINE-1, MSG_DONTWAIT);
                    }
                    else
                    {
                        break;
                    }
                }
                //printf("Recibido ack: %s",recvline2);
                //----------------------------------------------------------------------------------------------------------------------------------------

                // Una vez recibido el ack, cambiar variable para que el task handler continue
                //----------------------------------------------------------------------------------------------------------------------------------------
                //printf("Cambio ack en con %d\n",arguments->id);
                //printf("Conection %d; la dir de ack es %p\n",arguments->id,(void *)(arguments->ack_arg[arguments->id].ack));
                //printf("Task %d; la dir de ack lock es %p\n",arguments->id,(void *)(arguments->ack_arg[arguments->id].ack_lock));
                pthread_mutex_lock(arguments->ack_arg[arguments->id].ack_lock);
                *(arguments->ack_arg[arguments->id].ack) = 0;
                pthread_mutex_unlock(arguments->ack_arg[arguments->id].ack_lock);
                //----------------------------------------------------------------------------------------------------------------------------------------

                // Obtener del mesnaje el tam del archivo
                //----------------------------------------------------------------------------------------------------------------------------------------
                char fileSize[MAXLINE];
                strcpy(fileSize,recvline);
                fileSize[strcspn(fileSize,"\n")] = 0; 
                long unsigned int File_Size = (long unsigned int)atoi(fileSize);
                //----------------------------------------------------------------------------------------------------------------------------------------

                // Redireccionar archivo
                //----------------------------------------------------------------------------------------------------------------------------------------        
                long int rcvBytes = 0;
                long int BytesWritten = 0;
                while(File_Size != 0)
                {
                    if(*(arguments->salir) == 0) 
                    {
                        memset(recvline, 0, MAXLINE);  
                        rcvBytes = recv(connfd, recvline, MAXLINE-1, MSG_DONTWAIT);    
                        if(rcvBytes > 0)
                        {
                            WriteReturnValue = send(*(req->conn), recvline, (unsigned long int)rcvBytes, MSG_DONTWAIT);
                            if(WriteReturnValue > 0)
                            {
                                BytesWritten += WriteReturnValue;
                                File_Size -= (unsigned long int)WriteReturnValue;
                                //printf("Escritos %ld Bytes\n",BytesWritten);
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                //----------------------------------------------------------------------------------------------------------------------------------------
                // Al finalizar quitar el request de la lista y liberar el handler
                //----------------------------------------------------------------------------------------------------------------------------------------
                if(close(*(req->conn)) < 0)
                {
                    printf("Soy conection %d, Error al cerrar la conec %d\n",arguments->id,*(req->conn));
                    printf("El errno es %s\n",errnoname((int)errno));
                }
                else
                {
                    //printf("Soy conection %d, cierro el fd %d\n",arguments->id,*(req->conn));
                    pthread_mutex_lock(arguments->list_lock);
                    *(req->conn) = -1;
                    pthread_mutex_unlock(arguments->list_lock);
                }
                
                pthread_mutex_lock(arguments->list_lock);
                remove_req_list_head(arguments->list);
                pthread_mutex_unlock(arguments->list_lock);
                //----------------------------------------------------------------------------------------------------------------------------------------
                liberarHandler(arguments->availableConections,arguments->id,arguments->conections_lock);
            }
            
        } 
        else
        {
            break;
        }
        // Al finalizar quitar el request de la lista y liberar el handler
        //------------------------------------------------------------------------------------------------------------------------------------------------
        /*pthread_mutex_lock(arguments->list_lock);
        remove_req_list_head(arguments->list);
        pthread_mutex_unlock(arguments->list_lock);*/
        //------------------------------------------------------------------------------------------------------------------------------------------------
    }
    //printf("Salgo de conection %d\n",arguments->id);
    return NULL;    
}

    