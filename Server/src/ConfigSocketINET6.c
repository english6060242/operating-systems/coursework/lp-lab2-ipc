#include "Common.h"
#include "CommonFunctions.h"
#include "DataStructure.h"

void ServerConfigSocketINET6(int *sock, struct sockaddr_in6 *servaddr, int iport, long unsigned int max, char *stringaddr, char *interfaceName)
{
    // Create Socket
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if ((*(sock) = socket(AF_INET6, SOCK_STREAM, 0)) < 0)
    {
        printf("Error creating socket - server\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    int yes = 1;

    if (setsockopt(*(sock), SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1)
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    // Fill data to specify the type, port, and address of the socket
    //--------------------------------------------------------------------------------------------------------------------------------------------
    bzero(servaddr, sizeof(*servaddr)); // Clear servaddr structure, this is the same as memset(&servaddr, 0, sizeof(servaddr));
    servaddr->sin6_family = AF_INET6;   // It is a socket for communicating with remote processes via IPV6
    if (inet_pton(AF_INET6, stringaddr, &(servaddr->sin6_addr)) != 1)
    {
        printf("Incorrect address\n");
        exit(EXIT_FAILURE);
    }
    servaddr->sin6_port = htons((unsigned short int)iport);  // Get port from the arguments
    servaddr->sin6_scope_id = if_nametoindex(interfaceName); // enp2s0
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Associate the socket with the specified address
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if ((bind(*(sock), (SA *)servaddr, sizeof(*servaddr))) < 0)
    {
        printf("Error associating socket with INET6 address\n");
        printf("The errno is %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Listen on the socket
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if ((listen(*(sock), (int)max)) < 0)
    {
        printf("Error listening for messages\n");
        exit(EXIT_FAILURE);
    }
    // listen(int sockfd, int backlog) sets the socket specified by the fd sockfd as
    // passive, i.e., it waits for incoming connections to accept them using the accept() call.
    // The backlog argument refers to the maximum number of pending connections (there is a queue for these)
    //--------------------------------------------------------------------------------------------------------------------------------------------
}
