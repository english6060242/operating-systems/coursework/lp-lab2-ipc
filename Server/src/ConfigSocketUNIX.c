#include "Common.h"
#include "CommonFunctions.h"
#include "DataStructure.h"

void ServerConfigSocketUNIX(int *sock, struct sockaddr_un *servaddr, long unsigned int max, char *filename)
{    
    // Create Socket
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if ((*(sock) = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
    {
        printf("Error creating socket - server\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    unlink(filename);

    // Handle the address (file path) of the server to connect to
    //--------------------------------------------------------------------------------------------------------------------------------------------
    bzero(servaddr, sizeof(*servaddr)); // Clear servaddr structure
    servaddr->sun_family = AF_UNIX;
    strcpy(servaddr->sun_path, filename);
    //--------------------------------------------------------------------------------------------------------------------------------------------

    int yes = 1;

    if (setsockopt(*(sock), SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1)
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    // Associate the socket with the specified address
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if ((bind(*(sock), (SA *)servaddr, sizeof(*servaddr))) < 0)
    {
        printf("Error associating socket with UNIX address\n");
        printf("The errno is %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Listen on the socket
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if ((listen(*(sock), (int)max)) < 0) 
    {
        printf("Error listening for messages\n");
        exit(EXIT_FAILURE);
    }
    // listen(int sockfd, int backlog) sets the socket specified by the fd sockfd as
    // passive, i.e., it waits for incoming connections to accept them using the accept() call.
    // The backlog argument refers to the maximum number of pending connections (there is a queue for these)
    //--------------------------------------------------------------------------------------------------------------------------------------------
}
