#include "Common.h"
#include "CommonFunctions.h"
#include "DataStructure.h"
#include "ConnectionThread.h"

void* ConnectionPoolThreadCode(void *arg)
{
    // Create and initialize variables
    //--------------------------------------------------------------------------------------------------------------------------------------------
    struct Pool_arg_struct *arguments = arg;
    pthread_t *ConnectionThreads;
    struct Connection_arg_struct *conArgs;
    int availableConnections[5];
    pthread_mutexattr_t mta; // to configure lock attributes
    pthread_mutex_t *connections_lock;
    pthread_mutex_t *con_list_lock;
    db_request_list **l;

    ConnectionThreads = malloc((5 * sizeof(pthread_t)));
    conArgs = malloc((5 * sizeof(struct Connection_arg_struct)));
    connections_lock = malloc((5 * sizeof(pthread_mutex_t)));
    con_list_lock = malloc((5 * sizeof(pthread_mutex_t)));
    l = malloc((5 * sizeof(db_request_list)));

    pthread_mutexattr_init(&mta); // Initialize mta with default values
    pthread_mutexattr_setrobust(&mta, PTHREAD_MUTEX_ROBUST); // Robust: If a thread takes the lock and dies before releasing it, it's released automatically.
    pthread_mutexattr_settype(&mta, PTHREAD_MUTEX_RECURSIVE); // Recursive: This solves the error in the syscall when lock() is executed very frequently

    for (int i = 0; i < 5; i++)
    {
        l[i] = new_db_request_list();
    }

    for (int i = 0; i < 5; i++)
    {
        pthread_mutex_init(&connections_lock[i], &mta);
        pthread_mutex_init(&con_list_lock[i], &mta);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Initially, all connections are available
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for (int i = 0; i < 5; i++)
    {
        pthread_mutex_lock(&connections_lock[i]);
        availableConnections[i] = 1;
        pthread_mutex_unlock(&connections_lock[i]);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Fill arguments for connection threads
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for (int i = 0; i < 5; i++)
    {
        conArgs[i].id = i;
        strcpy(conArgs[i].IPV4_Server_Address, arguments->IPV4_Server_Address);
        conArgs[i].IPV4_iport = arguments->IPV4_iport;
        conArgs[i].availableConnections = availableConnections;
        conArgs[i].connections_lock = &connections_lock[i];
        conArgs[i].list_lock = &con_list_lock[i];
        conArgs[i].ack_arg = arguments->ack_arg;
        conArgs[i].list = l[i];
        conArgs[i].salir = arguments->salir;
    }

    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Launch threads to send/receive from the 5 connections
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for (int i = 0; i < 5; i++)
    {
        pthread_create(&ConnectionThreads[i], NULL, ConnectionThreadCode, &conArgs[i]);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    int isEmpty = 1; // To check if the request list is empty
    int availableConAmount = 0;
    int nextConnection = -1;

    while ((*arguments->salir) == 0)
    {
        // Wait until some TaskHandlingThread adds a request to the list
        //------------------------------------------------------------------------------------------------------------------------------------
        pthread_mutex_lock(arguments->req_list_lock);
        isEmpty = isEmpty_db_request_list(arguments->list);
        pthread_mutex_unlock(arguments->req_list_lock);

        while (isEmpty)
        {
            if ((*arguments->salir) == 0)
            {
                pthread_mutex_lock(arguments->req_list_lock);
                isEmpty = isEmpty_db_request_list(arguments->list);
                pthread_mutex_unlock(arguments->req_list_lock);
            }
            else
            {
                break;
            }
        }
        //------------------------------------------------------------------------------------------------------------------------------------

        if ((*arguments->salir) == 0)
        {
            // Wait until there are available connections
            //--------------------------------------------------------------------------------------------------------------------------------
            while (availableConAmount == 0)
            {
                if ((*arguments->salir) == 0)
                {
                    availableConAmount = getAvailableHandlersAmount(availableConnections, (long unsigned int)5, connections_lock);
                    /*if (availableConAmount == 0)
                    {
                        printf("All DB connections are busy\n");
                    }*/
                }
                else
                {
                    break;
                }
            }
            //--------------------------------------------------------------------------------------------------------------------------------

            if (*(arguments->salir) == 0)
            {
                // Get the number of the first available connection (in numerical order) and add the requests to the list of the thread corresponding to that
                // connection. Then remove the request from the intermediate list belonging to the pool.
                //----------------------------------------------------------------------------------------------------------------------------
                nextConnection = getFirstAvailableHandler(availableConnections, 5, connections_lock); // Get the first available connection in mutual exclusion
                availableConAmount = 0;
                while (nextConnection == -1)
                {
                    nextConnection = getFirstAvailableHandler(availableConnections, 5, connections_lock);
                }

                occupyHandler(availableConnections, nextConnection, &connections_lock[nextConnection]);

                if (nextConnection == 5)
                {
                    printf("There was next with 5\n");
                    exit(0);
                }

                pthread_mutex_lock(arguments->req_list_lock);
                int *auxConn = get_db_request(arguments->list, 0)->conn; // Get the socket number of the connection with the client that generated the req
                char auxString[MAXLINE];
                strcpy(auxString, get_db_request(arguments->list, 0)->sendmsg); // Copy the message from the request
                pthread_mutex_unlock(arguments->req_list_lock);
                // Add to the list (contained in the params of the thread corresponding to the obtained available connection) a new request (identical to the one
                // that was added to the pool list by TaskHandlingThread)
                pthread_mutex_lock(&con_list_lock[nextConnection]);
                add_db_request(conArgs[nextConnection].list, db_request_list_getNextID(conArgs[nextConnection].list), auxConn, auxString);
                pthread_mutex_unlock(&con_list_lock[nextConnection]);

                pthread_mutex_lock(arguments->req_list_lock);
                remove_req_list_head(arguments->list);
                pthread_mutex_unlock(arguments->req_list_lock);
                //----------------------------------------------------------------------------------------------------------------------------
            }
        }
        else
        {
            break;
        }
    }

    // Wait for the 5 threads that handle connections to the DB to finish
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for (int i = 0; i < 5; i++)
    {
        pthread_join(ConnectionThreads[i], NULL);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    for (int i = 0; i < 5; i++)
    {
        pthread_mutex_destroy(&connections_lock[i]);
        pthread_mutex_destroy(&con_list_lock[i]);
    }

    free(ConnectionThreads);
    free(conArgs);
    free(connections_lock);
    free(con_list_lock);
    free(l);

    return NULL;
}
