#include "Common.h"
#include "CommonFunctions.h"
#include "DataStructure.h"

void *ConnectionThreadCode(void *arg)
{
    // Variables
    //------------------------------------------------------------------------------------------------------------------------------------------------
    int connfd;
    struct sockaddr_in servaddr;
    struct Connection_arg_struct *arguments = arg;
    char sendmsg[MAXLINE];
    char recvline[MAXLINE];

    char recvline2[MAXLINE + 1];
    long int readBytes2;
    long unsigned int sendBytes; // Amount of Bytes to send
    long int readBytes;           // Amount of Bytes to receive
    long int WriteReturnValue;

    int isEmpty = 1;
    //------------------------------------------------------------------------------------------------------------------------------------------------

    //printf("I am connection %d\n", arguments->id);
    //print_db_request_list(*arguments->list);

    // Create Socket, AF_INET = Internet, SOCK_STREAM = Stream Socket, 0 = Default Protocol (TCP) (Connection to the DB)
    //------------------------------------------------------------------------------------------------------------------------------------------------
    if ((connfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("Failed to create socket with Pool\n");
        exit(EXIT_FAILURE);
    }
    *(arguments->ack_arg->ConnSocket) = connfd; // Save the socket number in the arguments to make it accessible by TaskHandlingThread
    //------------------------------------------------------------------------------------------------------------------------------------------------

    // Configure structure for connection
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //printf("Config con add: %s and port %d\n", arguments->IPV4_Server_Address, arguments->IPV4_iport);
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(arguments->IPV4_iport);
    if (inet_pton(AF_INET, arguments->IPV4_Server_Address, &servaddr.sin_addr) <= 0)
    {
        printf("Failed to convert IP address to binary with Pool\n");
        exit(EXIT_FAILURE);
    }
    //----------------------------------------------------------------------------------------------------------------------------------------------------

    // Connect to the server (DB) and handle errors
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    if (connect(connfd, (SA *)&servaddr, sizeof(servaddr)) < 0)
    {
        printf("Failed to connect to the server in connection_thread %d\n", arguments->id);
        exit(EXIT_FAILURE);
    }
    //----------------------------------------------------------------------------------------------------------------------------------------------------

    while (*(arguments->salir) == 0)
    {
        // Wait until there are requests in the queue to send to the db
        //------------------------------------------------------------------------------------------------------------------------------------------------
        pthread_mutex_lock(arguments->list_lock);
        isEmpty = isEmpty_db_request_list(arguments->list);
        pthread_mutex_unlock(arguments->list_lock);

        while (isEmpty)
        {
            if (*(arguments->salir) == 0)
            {
                pthread_mutex_lock(arguments->list_lock);
                isEmpty = isEmpty_db_request_list(arguments->list);
                pthread_mutex_unlock(arguments->list_lock);
            }
            else
            {
                break;
            }
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------
        if (*(arguments->salir) == 0)
        {
            // Once a request is entered, this connection is busy
            //--------------------------------------------------------------------------------------------------------------------------------------------

            // Get request info (message, socket connected to the client, etc.) in mutual exclusion
            //--------------------------------------------------------------------------------------------------------------------------------------------

            db_request *req = malloc(sizeof(db_request));
            char t = 'd';

            pthread_mutex_lock(arguments->list_lock);
            req = get_db_request(arguments->list, 0);
            pthread_mutex_unlock(arguments->list_lock);
            if (req != NULL)
            {
                //printf("I am connection %d the message of the req is %s\n",arguments->id,req->sendmsg);
                //hab = 0;
                //printf("I have to send the message AA%sAA\n",req->sendmsg);
                t = getTypeAndMessage(req->sendmsg, sendmsg);
                memset(sendmsg, 0, MAXLINE);
                strcpy(sendmsg, req->sendmsg);
            }
            else
            {
                //printf("Req is NULL in connection %d\n",arguments->id);
                exit(0);
            }
            //--------------------------------------------------------------------------------------------------------------------------------------------

            // Clients type A and B -> send the query to the db, receive and redirect the response
            //--------------------------------------------------------------------------------------------------------------------------------------------
            if ((t == 'a') || (t == 'b'))
            {
                // Get the length of the message, and send it to the db
                //----------------------------------------------------------------------------------------------------------------------------------------
                if (strcmp(sendmsg, "Type B | exit\n"))
                {
                    sendBytes = strlen(sendmsg);
                    WriteReturnValue = 0;
                    while (WriteReturnValue <= 0) // Wait until data is sent
                    {

                        if (*(arguments->salir) == 0)
                        {
                            WriteReturnValue = send(connfd, sendmsg, sendBytes, MSG_DONTWAIT);
                        }
                        else
                        {
                            break;
                        }
                    }
                    //----------------------------------------------------------------------------------------------------------------------------------------

                    // Receive the response from the db
                    //----------------------------------------------------------------------------------------------------------------------------------------
                    memset(recvline, 0, MAXLINE);
                    readBytes = 0;
                    while ((recvline[readBytes - 1] != '\n') && (recvline[readBytes - 2] != '\r')) // Detect end of response to the query
                    {
                        if (*(arguments->salir) == 0)
                        {
                            while (readBytes <= 0) // Wait until there is data
                            {
                                if (*(arguments->salir) == 0)
                                {
                                    readBytes = recv(connfd, recvline, MAXLINE - 1, MSG_DONTWAIT);
                                }
                                else
                                {
                                    break;
                                }
                            }
                            if ((recvline[readBytes - 1] == '\n') && (recvline[readBytes - 2] == '\r'))
                            {
                                break;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    //printf("Received from the DB: AA%sAA\n",recvline);
                    //----------------------------------------------------------------------------------------------------------------------------------------

                    // Redirect the message to the client
                    //----------------------------------------------------------------------------------------------------------------------------------------
                    memset(sendmsg, 0, MAXLINE);
                    strcpy(sendmsg, recvline);
                    sendBytes = strlen(sendmsg); // Calculate the number of characters in the message
                    WriteReturnValue = 0;
                    while (WriteReturnValue <= 0) // Wait until there is data
                    {
                        if (*(arguments->salir) == 0) // ver
                        {
                            if (*(req->conn) > 0)
                            {
                                WriteReturnValue = send(*(req->conn), sendmsg, sendBytes, MSG_DONTWAIT);
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    //WriteReturnValue = write(*(req->conn), sendmsg, sendBytes);
                    if ((WriteReturnValue == -1) || ((long unsigned int)WriteReturnValue != sendBytes))
                    {
                        printf("Failed to send message in connection Thread %d, fd = %d\n", arguments->id, *(req->conn));
                        printf("The errno is %s\n", errnoname((int)errno));
                        exit(EXIT_FAILURE);
                    }
                    //----------------------------------------------------------------------------------------------------------------------------------------

                }
                if (t == 'a' || (t == 'b' && (!strcmp(sendmsg, "Type B | exit\n"))))
                {
                    if (close(*(req->conn)) < 0)
                    {
                        printf("I am connection %d, Error closing the connection %d\n", arguments->id, *(req->conn));
                        printf("The errno is %s\n", errnoname((int)errno));
                    }
                    else
                    {
                        //printf("I am connection %d, I close the fd %d\n",arguments->id,*(req->conn));
                        pthread_mutex_lock(arguments->list_lock);
                        *(req->conn) = -1;
                        pthread_mutex_unlock(arguments->list_lock);
                    }
                }
                // When finished, remove the request from the list and release the handler
                //----------------------------------------------------------------------------------------------------------------------------------------
                pthread_mutex_lock(arguments->list_lock);
                remove_req_list_head(arguments->list);
                pthread_mutex_unlock(arguments->list_lock);

                releaseHandler(arguments->availableConnections, arguments->id, arguments->connections_lock);
                //----------------------------------------------------------------------------------------------------------------------------------------
            }
            //--------------------------------------------------------------------------------------------------------------------------------------------

            if (t == 'c')
            {
                //sendBytes = strlen(req->sendmsg); // Calculate the number of characters in the message
                sendBytes = strlen(sendmsg); // Calculate the number of characters in the message
                WriteReturnValue = 0;
                while (WriteReturnValue <= 0) // Wait until there is data
                {
                    if (*(arguments->salir) == 0)
                    {
                        WriteReturnValue = send(connfd, sendmsg, sendBytes, MSG_DONTWAIT);
                    }
                    else
                    {
                        break;
                    }
                }

                // Receive file size
                //----------------------------------------------------------------------------------------------------------------------------------------
                memset(recvline, 0, MAXLINE);
                readBytes = 0;
                while ((recvline[readBytes - 1] != '\n') && (recvline[readBytes - 2] != '\r')) // Detect end of the query
                {
                    if (*(arguments->salir) == 0)
                    {
                        while (readBytes <= 0) // Wait until there is data
                        {
                            readBytes = recv(connfd, recvline, MAXLINE - 1, MSG_DONTWAIT);
                        }
                        if ((recvline[readBytes - 1] == '\n') && (recvline[readBytes - 2] == '\r'))
                        {
                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                //----------------------------------------------------------------------------------------------------------------------------------------

                // Notify received size
                //----------------------------------------------------------------------------------------------------------------------------------------
                memset(sendmsg, 0, MAXLINE);
                strcat(sendmsg, "Size received\n");
                sendBytes = strlen(sendmsg); // Calculate the number of characters in the message
                WriteReturnValue = write(connfd, sendmsg, sendBytes);
                if ((WriteReturnValue == -1) || ((long unsigned int)WriteReturnValue != sendBytes))
                {
                    printf("Failed to send message\n");
                    printf("The errno is %s\n", errnoname((int)errno));
                    exit(EXIT_FAILURE);
                }
                //----------------------------------------------------------------------------------------------------------------------------------------

                // Send file size
                //----------------------------------------------------------------------------------------------------------------------------------------
                WriteReturnValue = 0;
                sendBytes = strlen(recvline);
                while (WriteReturnValue <= 0) // Wait until there is data
                {
                    if (*(arguments->salir) == 0)
                    {
                        //printf("Send size %s\n",recvline);
                        WriteReturnValue = send(*(req->conn), recvline, sendBytes, MSG_DONTWAIT);
                    }
                    else
                    {
                        break;
                    }
                }
                //----------------------------------------------------------------------------------------------------------------------------------------

                // Receive size ack
                //----------------------------------------------------------------------------------------------------------------------------------------
                readBytes2 = 0;
                memset(recvline2, 0, MAXLINE);
                while (readBytes2 != 14)
                {
                    if (*(arguments->salir) == 0)
                    {
                        
                        readBytes2 = recv(*(req->conn), recvline2, MAXLINE - 1, MSG_DONTWAIT);
                    }
                    else
                    {
                        break;
                    }
                }
                //printf("Received ack: %s\n",recvline2);
                //----------------------------------------------------------------------------------------------------------------------------------------

                // Once the ack is received, change the variable for the task handler to continue
                //----------------------------------------------------------------------------------------------------------------------------------------
                //printf("Change ack in con %d\n",arguments->id);
                //printf("Connection %d; the ack address is %p\n",arguments->id,(void *)(arguments->ack_arg[arguments->id].ack));
                //printf("Task %d; the ack lock address is %p\n",arguments->id,(void *)(arguments->ack_arg[arguments->id].ack_lock));
                pthread_mutex_lock(arguments->ack_arg[arguments->id].ack_lock);
                *(arguments->ack_arg[arguments->id].ack) = 0;
                pthread_mutex_unlock(arguments->ack_arg[arguments->id].ack_lock);
                //----------------------------------------------------------------------------------------------------------------------------------------

                // Get the file size from the message
                //----------------------------------------------------------------------------------------------------------------------------------------
                char fileSize[MAXLINE];
                strcpy(fileSize, recvline);
                fileSize[strcspn(fileSize, "\n")] = 0;
                long unsigned int File_Size = (long unsigned int)atoi(fileSize);
                //----------------------------------------------------------------------------------------------------------------------------------------

                // Redirect file
                //----------------------------------------------------------------------------------------------------------------------------------------
                long int rcvBytes = 0;
                long int BytesWritten = 0;
                while (File_Size != 0)
                {
                    if (*(arguments->salir) == 0)
                    {
                        memset(recvline, 0, MAXLINE);
                        rcvBytes = recv(connfd, recvline, MAXLINE - 1, MSG_DONTWAIT);
                        if (rcvBytes > 0)
                        {
                            WriteReturnValue = send(*(req->conn), recvline, (unsigned long int)rcvBytes, MSG_DONTWAIT);
                            if (WriteReturnValue > 0)
                            {
                                BytesWritten += WriteReturnValue;
                                File_Size -= (unsigned long int)WriteReturnValue;
                                //printf("Written %ld Bytes\n",BytesWritten);
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                //----------------------------------------------------------------------------------------------------------------------------------------
                // When finished, remove the request from the list and release the handler.
                //----------------------------------------------------------------------------------------------------------------------------------------
                if (close(*(req->conn)) < 0) {
                    printf("I am connection %d, Error closing connection %d\n", arguments->id, *(req->conn));
                    printf("The errno is %s\n", errnoname((int)errno));
                } else {
                    //printf("I am connection %d, closing fd %d\n", arguments->id, *(req->conn));
                    pthread_mutex_lock(arguments->list_lock);
                    *(req->conn) = -1;
                    pthread_mutex_unlock(arguments->list_lock);
                }

                pthread_mutex_lock(arguments->list_lock);
                remove_req_list_head(arguments->list);
                pthread_mutex_unlock(arguments->list_lock);
                //----------------------------------------------------------------------------------------------------------------------------------------
                releaseHandler(arguments->availableConnections, arguments->id, arguments->connections_lock);
            }

        } else {break;}
        // When finished, remove the request from the list and release the handler.
        //------------------------------------------------------------------------------------------------------------------------------------------------
        /*pthread_mutex_lock(arguments->list_lock);
        remove_req_list_head(arguments->list);
        pthread_mutex_unlock(arguments->list_lock);*/
        //------------------------------------------------------------------------------------------------------------------------------------------------
    }
    //printf("Exiting connection %d\n", arguments->id);
    return NULL;
}

    