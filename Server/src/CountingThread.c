#include "Common.h"
#include "CommonFunctions.h"
#include "DataStructure.h"

void* ThreadCode(void * arg)
{
    struct local_threads_arg_struct *arguments = arg;
    int EA,EP,count = 0;
    
    while(*(arguments->salir) == 0 && (arguments->ExitThread == 0))
    {
        if(*(arguments->salir) == 0 && (arguments->ExitThread == 0))
        {
            pthread_mutex_lock(arguments->lock);
            arguments->checksegs = 1;
            pthread_mutex_unlock(arguments->lock);
            EA = arguments->checksegs;
            usleep(1000000);
            EP = arguments->checksegs;
            count++;
            if(EA != EP)
            {
                count = 0;
            }
            if(count == 300)
            {
                pthread_mutex_lock(arguments->lock);
                arguments->ExitThread = 1;
                pthread_mutex_unlock(arguments->lock);
                //printf("15 segs\n");
                count = 0;
                break;
            }
        }
    }

    return NULL;
}