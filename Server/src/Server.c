#include "Common.h"
#include "CommonFunctions.h"
#include "DataStructure.h"
#include "Arg-Verify-Server.h"
#include "ConnectionPoolThread.h"
#include "ConnectionThread.h"
#include "CountingThread.h"
#include "TaskHandlingThread.h"
#include "ConfigSocketINET.h"
#include "ConfigSocketUNIX.h"
#include "ConfigSocketINET6.h"
#include "Server_INET_Stream.h"
#include "Server_UNIX_Stream.h"
#include "Server_INET6_Stream.h"

int main(int argc, char *argv[])
{    
    // Variables: Threads and argument structures for the three protocols
    //--------------------------------------------------------------------------------------------------------------------------------------------
    pthread_t INET_Server_Thread;
    pthread_t UNIX_Server_Thread;
    pthread_t INET6_Server_Thread;

    struct INET_arg_struct INET_arguments;
    struct UNIX_arg_struct UNIX_arguments; 
    struct INET6_arg_struct INET6_arguments; 

    char file_path[MAXLINE]; // Path for the UNIX file
    char file_name[MAXLINE]; // Name for the UNIX file
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Variables: Arguments for the threads handling the 5 DB connections
    //--------------------------------------------------------------------------------------------------------------------------------------------
    int ConnSocket[5];
    int ack[5];
    pthread_mutexattr_t mta; // to configure lock attributes
    pthread_mutex_t ack_lock[5];
    struct ack_arg_struct ackArgs[5];

    pthread_mutexattr_init(&mta); // Initialize mta with default values
    pthread_mutexattr_setrobust(&mta, PTHREAD_MUTEX_ROBUST); // Robust: If a thread takes the lock and dies before releasing it, it is automatically released.
    pthread_mutexattr_settype(&mta, PTHREAD_MUTEX_RECURSIVE); // Recursive: This solves the error in the syscall when lock() is called too frequently

    for(int i = 0; i < 5; i++)
    {
        //ack_lock[i] = PTHREAD_MUTEX_INITIALIZER;
        ack[i] = 1;
        //ack_lock[i] = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
        pthread_mutex_init(&ack_lock[i], &mta); 
        ackArgs[i].ack = &(ack[i]);
        ackArgs[i].ack_lock = &(ack_lock[i]);
        ackArgs[i].ConnSocket = &(ConnSocket[i]);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Variables
    //--------------------------------------------------------------------------------------------------------------------------------------------
    char salir[MAXLINE];
    int exitAllThreads = 0;
    pthread_t connPool;
    struct Pool_arg_struct cp_args;
    //pthread_mutex_t conections_lock;
    pthread_mutex_t req_list_lock;
    db_request_list *l = new_db_request_list();
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Configure and initialize mutex
    //--------------------------------------------------------------------------------------------------------------------------------------------
    pthread_mutex_init(&req_list_lock, &mta); 
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verify arguments and load them where appropriate
    //--------------------------------------------------------------------------------------------------------------------------------------------
    verifyArguments(argc, argv);
    strcpy(cp_args.IPV4_Server_Address, argv[7]);
    cp_args.IPV4_iport = (unsigned short int)atoi(argv[8]);
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Load the rest of the arguments for connection pool
    //--------------------------------------------------------------------------------------------------------------------------------------------
    //cp_args.availableConections = availableConections;
    cp_args.salir = &exitAllThreads;
    cp_args.list = l;
    cp_args.req_list_lock = &req_list_lock;
    cp_args.ack_arg = ackArgs;
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Launch connections pool thread
    //--------------------------------------------------------------------------------------------------------------------------------------------
    pthread_create(&connPool, NULL, ConnectionPoolThreadCode, &cp_args);
    //--------------------------------------------------------------------------------------------------------------------------------------------

    strcpy(INET_arguments.IPV4_Server_Address, argv[1]);
    INET_arguments.IPV4_iport = (short unsigned int)atoi(argv[2]);
    INET_arguments.req_list_lock = &req_list_lock;
    INET_arguments.list = l;
    INET_arguments.maxClients = atoi(argv[9]);
    INET_arguments.salir = &exitAllThreads;
    INET_arguments.ack_arg = ackArgs;

    strcpy(file_path, argv[3]);
    strcpy(file_name, file_path);
    strcat(file_name, "UNIX_FILE");
    if (remove(file_name) == 0)      
    {                               
        rmdir(file_path);
    }
    if (mkdir(file_path, 0777) < 0)
    {
        printf("Error creating the directory for the UNIX file\n");
        printf("The errno is %s\n", errnoname((int)errno));
        exit(EXIT_FAILURE);
    }
    strcpy(UNIX_arguments.UNIX_File_Name, file_name);
    UNIX_arguments.req_list_lock = &req_list_lock;
    UNIX_arguments.list = l;
    UNIX_arguments.maxClientes = atoi(argv[9]);
    UNIX_arguments.salir = &exitAllThreads;

    strcpy(INET6_arguments.IPV6_Server_Address, argv[4]);
    INET6_arguments.IPV6_iport = (short unsigned int)atoi(argv[5]);
    strcpy(INET6_arguments.IPV6_Interface, argv[6]);
    INET6_arguments.req_list_lock = &req_list_lock;
    INET6_arguments.list = l;
    INET6_arguments.maxClientes = atoi(argv[9]);
    INET6_arguments.salir = &exitAllThreads;

    //--------------------------------------------------------------------------------------------------------------------------------------------
    //printf("reached here\n");
    // Launch threads
    //--------------------------------------------------------------------------------------------------------------------------------------------
    // INET
    pthread_create(&INET_Server_Thread, NULL, INET_Server_Code, &INET_arguments);
    // UNIX
    pthread_create(&UNIX_Server_Thread, NULL, UNIX_Server_Code, &UNIX_arguments);
    // INET6
    pthread_create(&INET6_Server_Thread, NULL, INET6_Server_Code, &INET6_arguments);
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Get the value entered from stdin, and if "salir" is entered, modify the value that causes all threads to end
    //--------------------------------------------------------------------------------------------------------------------------------------------
    while (!exitAllThreads)
    {
        printf("Enter 'salir' to close the server program\n");
        safeGetString(salir, MAXLINE);
        if (!strcmp(salir, "salir\n"))
        {
            exitAllThreads = 1;
        }
        else
        {
            printf("You entered %s\n", salir);
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Wait for all threads to finish before allowing main to finish so that connections are closed, memory is freed, etc.
    //--------------------------------------------------------------------------------------------------------------------------------------------
    pthread_join(INET_Server_Thread, NULL);
    pthread_join(UNIX_Server_Thread, NULL);
    pthread_join(INET6_Server_Thread, NULL);
    pthread_join(connPool, NULL);
    //--------------------------------------------------------------------------------------------------------------------------------------------

    for (int i = 0; i < 5; i++)
    {
        pthread_mutex_destroy(&ack_lock[i]);
    }
    pthread_mutex_destroy(&req_list_lock);

    if (remove(file_name) < 0)
    {
        printf("Error deleting the UNIX file\n");
        printf("The errno is %s\n", errnoname((int)errno));
        exit(EXIT_FAILURE);
    }
    if (rmdir(file_path) < 0)
    {
        printf("Error deleting the UNIX path\n");
        printf("The errno is %s\n", errnoname((int)errno));
        exit(EXIT_FAILURE);
    }

    return 0;
}
