#include "Common.h"
#include "CommonFunctions.h"
#include "DataStructure.h"
#include "ConfigSocketINET6.h"
#include "TaskHandlingThread.h"
#include "CountingThread.h"

void *INET6_Server_Code(void *arg)
{
    struct INET6_arg_struct *arguments = arg; // Retrieve arguments
    
    // Variables: Create / configure sockets
    //--------------------------------------------------------------------------------------------------------------------------------------------
    int listenfd; // File descriptor for the socket that listens for connections.
    int *connfd; // File descriptor for the socket from accept() (connection established)
    struct sockaddr_in6 servaddr; // Structure to specify the server address

    connfd = malloc((unsigned long int)arguments->maxClientes * sizeof(int));
    for(int i = 0; i < arguments->maxClientes; i++)
    {
        connfd[i] = -1;
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // Variables: Create threads to handle connections | Allocate memory: as many TaskThreads, CountingThreads, and arguments as maxClientes
    //--------------------------------------------------------------------------------------------------------------------------------------------
    pthread_t *TaskThread;
    pthread_t *CountingThread;
    struct local_threads_arg_struct *Handler_Thread_Args;
    pthread_mutexattr_t mta; // to configure lock attributes
    pthread_mutex_t *handlers_lock;
    pthread_mutex_t lock;

    pthread_mutexattr_init(&mta); // Initialize mta with default values
    pthread_mutexattr_setrobust(&mta,PTHREAD_MUTEX_ROBUST); // Robust: If a thread takes the lock and dies before releasing it, it is automatically released.
    pthread_mutexattr_settype(&mta,PTHREAD_MUTEX_RECURSIVE); // Recursive: This solves the error in the syscall when lock() is called too frequently
    
    pthread_mutex_init(&lock,&mta); 

    TaskThread = malloc((unsigned long int)arguments->maxClientes * sizeof(pthread_t)); 
    CountingThread = malloc((unsigned long int)arguments->maxClientes * sizeof(pthread_t)); 
    Handler_Thread_Args = malloc((unsigned long int)arguments->maxClientes * sizeof(struct local_threads_arg_struct));
    handlers_lock = malloc((unsigned long int)arguments->maxClientes * sizeof(pthread_mutex_t));

    for(int i = 0; i <arguments->maxClientes; i++)
    {
        pthread_mutex_init(&handlers_lock[i],&mta); 
        
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // Variables: Limit the number of handler threads that can be launched and determine the first available one. Allocate memory
    //--------------------------------------------------------------------------------------------------------------------------------------------
    long unsigned int CurrentAHAmount = 0; // Number of available handlers at a given moment
    int *AvailableHandlers; // Array of integers serving as flags, if AvailableHandlers[i] == 1 -> handler i is available
    int nextHandler; // This integer is evaluated with the first available handler at a given moment

    AvailableHandlers = malloc((unsigned long int)arguments->maxClientes * sizeof(int)); // Allocate memory: The array will have as many elements as maxClientes
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Configure socket
    //--------------------------------------------------------------------------------------------------------------------------------------------
    ServerConfigSocketINET6(&listenfd, &servaddr, arguments->IPV6_iport, (unsigned long int)arguments->maxClientes, arguments->IPV6_Server_Address, 
    arguments->IPV6_Interface);
    //--------------------------------------------------------------------------------------------------------------------------------------------
     
    // Initially all handlers are available, fill the array of indicators with "1" -> "Available"
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(int i = 0; i < arguments->maxClientes; i++)
    {
        AvailableHandlers[i] = 1;
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // The thread for INET6 is always waiting for connections and launching threads to handle them when they are established
    //--------------------------------------------------------------------------------------------------------------------------------------------
    while(*(arguments->salir) == 0) // If "salir" is entered in main, no more connections are expected
    {   
        // Get the number of handlers available. If there are already as many threads working as maxClientes, wait until one finishes
        //----------------------------------------------------------------------------------------------------------------------------------------// Accessed in mutual exclusion, as handler threads modify AvailableHandlers by releasing a handler when they finish
        CurrentAHAmount = (long unsigned int)getAvailableHandlersAmount(AvailableHandlers,(unsigned long int)arguments->maxClientes,handlers_lock);

        if(CurrentAHAmount == 0 || getFirstAvailableHandler(AvailableHandlers,(unsigned long int)arguments->maxClientes,handlers_lock) < 0)
        {
            printf("No UNIX handlers available, wait until one becomes available\n");
            while(CurrentAHAmount == 0)
            {
                if(*(arguments->salir) == 0) 
                {
                    CurrentAHAmount = (long unsigned int)getAvailableHandlersAmount(AvailableHandlers,(unsigned long int)arguments->maxClientes,handlers_lock);
                }
                else
                {
                    break;
                }
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------------
        
        // Knowing that there are handlers available, get the first one available in numerical order
        //----------------------------------------------------------------------------------------------------------------------------------------
        if(*(arguments->salir) == 0) 
        {
            nextHandler = getFirstAvailableHandler(AvailableHandlers,(unsigned long int)arguments->maxClientes,handlers_lock);
        }
        CurrentAHAmount = 0;
        //----------------------------------------------------------------------------------------------------------------------------------------

        // Wait for a connection and handle the error in case it occurs. Use fd conn[nextHandler] -> first available handler
        //----------------------------------------------------------------------------------------------------------------------------------------
        int flags = fcntl(listenfd, F_GETFL, 0);       // Configure so that accept() becomes
        fcntl(listenfd, F_SETFL, flags | O_NONBLOCK);  // non-blocking.
        while(connfd[nextHandler] < 0)
        {
            if(*(arguments->salir) == 0)
            {
                connfd[nextHandler] = accept(listenfd, (SA *) NULL, NULL); // NULL -> It doesn't matter who connects, accept the connection
                /*if(connfd[nextHandler] == -1) // I no longer check the error because my accept is non-blocking and I wait in this while
                {                               // until a correct connection is established
                    printf("Error in accept()\n");
                    exit(EXIT_FAILURE);
                }*/
            }
            else
            {
                break;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------------
        
        
        // Now that there is a connection, fill in the arguments, launch the handler thread for this connection, and modify AvailableHandlers in mutual exclusion
        //----------------------------------------------------------------------------------------------------------------------------------------
        //printf("Connection established. Handled by thread %d\n",nextHandler); 
        if(*(arguments->salir) == 0)
        {
            Handler_Thread_Args[nextHandler].id = nextHandler;
            Handler_Thread_Args[nextHandler].checksegs = 0;
            Handler_Thread_Args[nextHandler].ConnSocket = &(connfd[nextHandler]);
            Handler_Thread_Args[nextHandler].ExitThread = 0;
            Handler_Thread_Args[nextHandler].Handlers = AvailableHandlers;
            Handler_Thread_Args[nextHandler].handlers_lock = &handlers_lock[nextHandler];
            Handler_Thread_Args[nextHandler].lock = &lock;
            Handler_Thread_Args[nextHandler].req_list_lock = arguments->req_list_lock;
            Handler_Thread_Args[nextHandler].list = arguments->list;
            Handler_Thread_Args[nextHandler].salir = arguments->salir;
            
            occupyHandler(AvailableHandlers,nextHandler,&handlers_lock[nextHandler]);
            
            pthread_create(&(TaskThread[nextHandler]),NULL,Task,&(Handler_Thread_Args[nextHandler]));
            pthread_create(&(CountingThread[nextHandler]),NULL,ThreadCode,&(Handler_Thread_Args[nextHandler]));

        }
        
        //----------------------------------------------------------------------------------------------------------------------------------------
    }

    // If you want to exit the server, wait for all threads launched by this INET6 thread to finish (these threads end if salir == 1)
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(*(arguments->salir) == 1)
    {
        for(int i = 0; i < arguments->maxClientes; i++)
        {
            pthread_join(TaskThread[i],NULL);
            pthread_join(CountingThread[i],NULL);
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Then close the fds, check for errors, and free all allocated memory
    //--------------------------------------------------------------------------------------------------------------------------------------------
          
    if((close(listenfd) < 0)) // Close the connection listening fd
    {
        printf("Error closing listenfd\n");
        exit(EXIT_FAILURE); 
    }

    for(int i = 0; i < arguments->maxClientes; i++)
    {
        pthread_mutex_destroy(&handlers_lock[i]);
    }
    pthread_mutex_destroy(&lock);

    free(AvailableHandlers);   // Free the array indicator of available handlers
    free(TaskThread);          // Free the array of threads to handle connections 
    free(CountingThread);      // Free the array of threads to close handler threads via timeout 
    free(connfd);              // Free the array of fds for connections
    free(handlers_lock);
    free(Handler_Thread_Args); // Free the array of arguments for handler threads
    //--------------------------------------------------------------------------------------------------------------------------------------------

    return NULL;
}
