#include "Common.h"
#include "CommonFunctions.h"
#include "DataStructure.h"

void* Task(void * arg)
{
    struct local_threads_arg_struct *arguments = arg;
    
    char recvline[MAXLINE+1]; // Buffer for receiving. It's MAXLINE + 1 because...
    char aux[MAXLINE];         // Auxiliary to remove checksum and end of message
    long int readBytes;

    // Variables to wait for the connection being used to receive the ack
    //--------------------------------------------------------------------------------------------------------------------------------------------
    int waitACK = 1;
    int conNumber;
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    //printf("I am task %d with socketcon %d\n", arguments->id, *(arguments->ConnSocket));

    while ((arguments->ExitThread == 0) && (*(arguments->salir) == 0)) 
    {
        // Read message: Initially readBytes is zero, as long as we don't want to exit, call recv. If readBytes > 0 -> data has been received.
        //----------------------------------------------------------------------------------------------------------------------------------------
        if ((arguments->ExitThread == 0) && (*(arguments->salir) == 0))
        {
            readBytes = 0;
            while (readBytes <= 0)
            {
                if ((arguments->ExitThread == 0) && (*(arguments->salir) == 0))
                {
                    readBytes = recv(*(arguments->ConnSocket), recvline, MAXLINE-1, MSG_DONTWAIT);
                }
                //if(arguments->ExitThread) // If the time runs out or a read error occurs (which could be due to timeout), exit
                else
                {
                    /*if(readBytes < 0) // I don't handle the error anymore because I use non-blocking recv and wait until a correct result is obtained
                    {
                        printf("Read error in task %d\n", arguments->id);
                        printf("The errno is %s\n", errnoname((int)errno));
                    }*/
                    if (*(arguments->ConnSocket) != -1)
                    {
                        if (close(*(arguments->ConnSocket)) < 0)
                        {
                            printf("I am task %d, Error closing connection %d\n", arguments->id, *(arguments->ConnSocket));
                            printf("The errno is %s\n", errnoname((int)errno));
                        }
                        else
                        {
                            //printf("I am task %d, close fd %d\n", arguments->id, *(arguments->ConnSocket));
                            *(arguments->ConnSocket) = -1;
                        }
                    }
                    //*(arguments->ConnSocket) = -1;
                    break;
                }
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------------

        //
        //----------------------------------------------------------------------------------------------------------------------------------------
        if (!arguments->ExitThread && *(arguments->salir) == 0)
        {
            if (readBytes > 0)
            {
                // If data has been received, modify checksegs to reset the timer that closes this thread
                //--------------------------------------------------------------------------------------------------------------------------------
                if ((arguments->ExitThread == 0) && (*(arguments->salir) == 0))
                {
                    pthread_mutex_lock(arguments->lock);
                    arguments->checksegs = 0;
                    pthread_mutex_unlock(arguments->lock);
                }
                //--------------------------------------------------------------------------------------------------------------------------------
                
                // Detect end of received data (the end of http messages usually has \n\r\n\r, when detecting \n we can infer -> end
                //--------------------------------------------------------------------------------------------------------------------------------
                if (recvline[readBytes - 1] == '\n')   
                {                                   
                    // Get the client type from which the message comes
                    //----------------------------------------------------------------------------------------------------------------------------
                    char pureMsg[MAXLINE];
                    char t;
                    if ((arguments->ExitThread == 0) && (*(arguments->salir) == 0))
                    {
                        t = getTypeAndMessage(recvline, pureMsg);
                    }
                    //----------------------------------------------------------------------------------------------------------------------------
                    
                    // If the client is type C, the message must be added to the requests queue, but NOT the ack of the size for the handshake: 
                    // size-> ; <-ack ; -> file
                    //----------------------------------------------------------------------------------------------------------------------------
                    if (t == 'c')
                    {
                        //printf("It is type C\n");
                        // Verify that the message is not an ack (Received Size) and not ""
                        //------------------------------------------------------------------------------------------------------------------------
                        if ((strcmp(recvline, "Size received\n")) && (recvline[0] != '\0')) // if it's not an ack, add it to the list
                        {
                            strcpy(aux, recvline);
                            pthread_mutex_lock(arguments->req_list_lock);
                            add_db_request(arguments->list, db_request_list_getNextID(arguments->list), arguments->ConnSocket, aux);
                            pthread_mutex_unlock(arguments->req_list_lock);
                            // After adding the message, you must wait until the connection thread receives the ack, for that, the array is used
                            // called waitACK (initially waitACK[i] is 1 and upon receiving the ack, thread i, waitACK[i] becomes zero). 
                            
                            // First it is determined that element i of waitACK[] corresponds to the thread working with this
                            // connection (using the socket number of the client connection)
                            //--------------------------------------------------------------------------------------------------------------------
                            for (int i = 0; i < 5; i++)
                            {
                                if (*(arguments->ack_arg[i].ConnSocket) == *(arguments->ConnSocket))
                                {
                                    conNumber = i;
                                }
                            }
                            //--------------------------------------------------------------------------------------------------------------------
                            
                            // Wait until the conectionThread receives the ack, if this thread does not wait, the ack will be extracted by this thread
                            //--------------------------------------------------------------------------------------------------------------------
                            while (waitACK) 
                            {
                                if ((arguments->ExitThread == 0) && (*(arguments->salir) == 0))
                                {
                                    pthread_mutex_lock(arguments->ack_arg[conNumber].ack_lock);
                                    waitACK = *(arguments->ack_arg[conNumber].ack);
                                    pthread_mutex_unlock(arguments->ack_arg[conNumber].ack_lock);
                                }
                                else
                                {
                                    break;
                                }
                            }
                            if (!waitACK)
                            {
                                //printf("Task %d; Received ack\n", arguments->id);
                                // ack = 1 then, in client type C, conectionThread receives a message from the client instead of taskHandlingThread (which waits for conectionThread)
                                pthread_mutex_lock(arguments->ack_arg[conNumber].ack_lock);
                                *(arguments->ack_arg[conNumber].ack) = 1;
                                pthread_mutex_unlock(arguments->ack_arg[conNumber].ack_lock);
                            }
                            //--------------------------------------------------------------------------------------------------------------------
                        }
                        //------------------------------------------------------------------------------------------------------------------------
                        //freeHandler(arguments->Handlers, arguments->id, arguments->lock);
                        pthread_mutex_lock(arguments->lock);
                        arguments->ExitThread = 1;
                        pthread_mutex_unlock(arguments->lock);
                        //printf("Task %d changes ExitThread\n", arguments->id);
                    }
                    //----------------------------------------------------------------------------------------------------------------------------
                    // If it is a client type A or B, simply add the message to the requests queue
                    if (t == 'b')
                    {
                      //printf("It is type B\n");
                      if ((arguments->ExitThread == 0) && (*(arguments->salir) == 0))
                        {
                            strcpy(aux, recvline);
                            if (!strcmp(aux, "Type B | quit\n"))
                            {
                                pthread_mutex_lock(arguments->lock);
                                arguments->ExitThread = 1;
                                pthread_mutex_unlock(arguments->lock); 
                                //printf("We have to exit task\n");
                            }
                            pthread_mutex_lock(arguments->req_list_lock);
                            add_db_request(arguments->list, db_request_list_getNextID(arguments->list), arguments->ConnSocket, aux);
                            pthread_mutex_unlock(arguments->req_list_lock);
                        }
                    }
                    if (t == 'a')
                    {
                        //printf("It is type A\n");
                        if ((arguments->ExitThread == 0) && (*(arguments->salir) == 0))
                        {
                            strcpy(aux, recvline);
                            pthread_mutex_lock(arguments->req_list_lock);
                            add_db_request(arguments->list, db_request_list_getNextID(arguments->list), arguments->ConnSocket, aux);
                            pthread_mutex_unlock(arguments->req_list_lock);
                        }
                        pthread_mutex_lock(arguments->lock);
                        arguments->ExitThread = 1;
                        pthread_mutex_unlock(arguments->lock);
                    }
                    //----------------------------------------------------------------------------------------------------------------------------
                    
                    // Clear readBytes and recvline so that there are no data from previous messages
                    //----------------------------------------------------------------------------------------------------------------------------
                    readBytes = 0;
                    memset(recvline, 0, MAXLINE); 
                    //----------------------------------------------------------------------------------------------------------------------------
                }
                //--------------------------------------------------------------------------------------------------------------------------------
            }
        }
        else
        {
            break;
        }
        
    }

    while (*(arguments->ConnSocket) != -1){}

    releaseHandler(arguments->Handlers, arguments->id, arguments->handlers_lock);

    return NULL;
}
